<?php
/*
Intense Template Name: Video
*/

$intense_post_media = Intense_Post_Media::get_metadata();

if ( isset( $intense_post_media['video'] ) ) {
	$video = $intense_post_media['video'];

	if ( $video['type'] == 'wordpress' ) {
		echo intense_run_shortcode( 'intense_video', array(
			'video_type' => $video['type'],
			'video_size' => '',
			'poster' => $video['poster'],
			'mp4' => $video['mp4'],
			'ogv' => $video['ogv'],
			'webm' => $video['webm']
		) );
	} else {
		echo intense_run_shortcode( 'intense_video', array(
			'video_type' => $video['type'],
			'video_url' => esc_url( $video['url'] ),
			'video_size' => ''
		) );
	}
}
