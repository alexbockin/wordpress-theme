<?php
/*
Intense Template Name: Image
*/

global $intense_post_media_current_image;

$intense_post_media = Intense_Post_Media::get_metadata();

$image_settings = $intense_post_media['image'];

if ( !isset( $intense_post_media_current_image ) && count( $image_settings['images'] ) > 0 ) {
	$image_values = array_values( $image_settings['images'] );
	$intense_post_media_current_image = $image_values[0];
}

$atts = array(	
	'type' => $image_settings['type'],
	'size' => $image_settings['size'],
	'lightbox_type' => isset( $intense_post_media['lightbox_type'] ) ? $intense_post_media['lightbox_type'] : null,
	'title' => $intense_post_media['post_title'],
	'caption' => isset( $intense_post_media_current_image['caption'] ) ? $intense_post_media_current_image['caption'] : null,
	'border_radius' => $intense_post_media['border_radius'],
	'border' => $intense_post_media['border'],
	'border_top' => $intense_post_media['border_top'],
	'border_bottom' => $intense_post_media['border_bottom'],
	'border_left' => $intense_post_media['border_left'],
	'border_right' => $intense_post_media['border_right'],
	'box_shadow' => $intense_post_media['box_shadow'],
	'effect' => isset( $image_settings['caman'] ) ? $image_settings['caman']['effect'] : null,
	'splits' => isset( $image_settings['picstrip'] ) ? $image_settings['picstrip']['splits'] : null,
	'hgutter' => isset( $image_settings['picstrip'] ) ? $image_settings['picstrip']['hgutter'] : null,
	'vgutter' => isset( $image_settings['picstrip'] ) ? $image_settings['picstrip']['vgutter'] : null,
	'bgcolor' => isset( $image_settings['picstrip'] ) ? $image_settings['picstrip']['bgcolor'] : null,
	'starteffect' => isset( $image_settings['adipoli'] ) ? $image_settings['adipoli']['starteffect'] : null,
	'hovereffect' => isset( $image_settings['adipoli'] ) ? $image_settings['adipoli']['hovereffect'] : null,
	'linkurl' => isset( $intense_post_media['include_link'] ) && $intense_post_media['include_link'] ? $intense_post_media['permalink'] : null,
);

if ( !empty( $image_settings['hover_effect'] ) ) {
	$atts['hover_effect'] = $image_settings['hover_effect']['type'];

	switch ( $image_settings['hover_effect']['type'] ) {
		case 'effeckt':
			$atts['effeckt'] = $image_settings['hover_effect']['mode'];
			$atts['effecktcolor'] = isset( $image_settings['hover_effect']['color'] ) ? $image_settings['hover_effect']['color'] : null;
			$atts['effecktopacity'] = isset( $image_settings['hover_effect']['opacity'] ) ? $image_settings['hover_effect']['opacity'] : null;
			break;
		case 'subtle':
			$atts['subtleeffect'] = $image_settings['hover_effect']['mode'];
			break;
	}
}

//remove empty values from array
$atts = array_filter( $atts );

if ( isset( $intense_post_media_current_image ) ) {	
	$atts['image'] = $intense_post_media_current_image['id'];
} else if ( isset( $intense_post_media['show_missing_image'] ) && $intense_post_media['show_missing_image'] ) {
	$atts['image'] = $intense_post_media['missing_image_url'];
}

if ( $intense_post_media['show_shadow'] && ( empty( $intense_post_media['is_slider'] ) || !$intense_post_media['is_slider'] ) ) {	
	$atts['shadow'] = $intense_post_media['shadow'];	
}

if ( isset( $intense_post_media_current_image['itemprop'] ) ) {
	$atts['itemprop'] = $intense_post_media_current_image['itemprop'];
}

if ( !empty( $atts['image'] ) ) {	
	echo intense_run_shortcode( 'intense_image', $atts );
}

$intense_post_media_current_image = null;
