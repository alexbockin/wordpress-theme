<?php
/*
Intense Template Name: Standard Priority
*/

$intense_post_media = Intense_Post_Media::get_metadata();

//echo '<pre>'; print_r($intense_post_media); echo '</pre>';

if ( isset( $intense_post_media['video'] ) && isset( $intense_post_media['show_video'] ) && $intense_post_media['show_video'] ) {
	require "video.php";
} else if ( isset( $intense_post_media['audio'] ) && isset( $intense_post_media['show_audio'] ) && $intense_post_media['show_audio'] ) {
	require "audio.php";
} else {
	if ( !empty( $intense_post_media['is_slider'] ) && $intense_post_media['is_slider'] 
		&& count( $intense_post_media['image']['images'] ) > 1 
		&& ( empty( $intense_post_media['show_only_first'] ) || !$intense_post_media['show_only_first'] ) ) {

		require "slider.php";
	} else {
		require "image.php";
	}
}