<?php
/*
Intense Template Name: Audio
*/

$intense_post_media = Intense_Post_Media::get_metadata();

if ( isset( $intense_post_media['audio'] ) ) {
	$audio = $intense_post_media['audio'];
	
	intense_run_shortcode( 'intense_audio', array(
		'url' => esc_url( $audio['url'] ),
		'width' => '100%',
		'autoplay' => '0',
		'loop' => '0'
	));
}
