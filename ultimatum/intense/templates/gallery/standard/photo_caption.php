<?php
/*
Intense Template Name: Captioned Photo
*/

if ( empty( $class ) ) $class = 'intense';

if ( $lazy_load  ) {
    intense_add_script( 'lazyloadxt' );
    $image_src_attr = 'data-src';
} else {
    $image_src_attr = 'src';
}

Intense()->add_dynamic_css( 'gallery_caption_template', "
	.photo-caption-wrapper {
		width:100%; 
		display: inline-block; 
		position: relative;
	}

	.photo-caption {
		position: absolute; 
		background: rgba(255,255,255,.8); 
		bottom: 0; 
		left: 0; 
		right: 0; 
		text-align: center; 
		color: #444; 
		padding: 3px 0;
	}
");

?>
<div class="photo-caption-wrapper">
<img 
	class="<?php echo esc_attr( $class ); ?>"
	<?php echo esc_attr( $width ) . esc_attr( $height ); ?>
	style="<?php echo esc_attr( $radius ); ?> padding:0;<?php echo esc_attr( $style ); ?>"
	<?php echo $image_src_attr; ?>="<?php echo esc_url( $url ); ?>"
	title="<?php echo esc_attr( $title ); ?>"
	alt="<?php echo esc_attr( $alt ); ?>"
	width="<?php echo esc_attr( $width ); ?>"
	height="<?php echo esc_attr( $height ); ?>"
	<?php if ( !empty( $animation ) ) : ?> data-effect="<?php echo esc_attr( $animation ); ?>"<?php endif; ?>
/>
<div class="photo-caption"><?php echo esc_attr( $title ); ?></div>
</div>
