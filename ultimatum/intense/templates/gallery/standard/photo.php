<?php
/*
Intense Template Name: Standard Photo
*/

if ( empty( $class ) ) $class = 'intense';

if ( $lazy_load ) {
    intense_add_script( 'lazyloadxt' );
    $image_src_attr = 'data-src';
} else {
    $image_src_attr = 'src';
}
?>

<img 
	class="<?php echo esc_attr( $class ); ?>"
	<?php echo $width . $height; ?>
	style="<?php echo esc_attr( $radius ); ?> padding:0;<?php echo esc_attr( $style ); ?>"
	<?php echo $image_src_attr; ?>="<?php echo esc_url( $url ); ?>"
	title="<?php echo esc_attr( $title ); ?>"
	alt="<?php echo esc_attr( $alt ); ?>"
	width="<?php echo esc_attr( $width ); ?>"
	height="<?php echo esc_attr( $height ); ?>"
	<?php if ( !empty( $animation ) ) : ?> data-effect="<?php echo esc_attr( $animation ); ?>"<?php endif; ?>
/>