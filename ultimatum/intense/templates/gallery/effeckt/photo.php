<?php
/*
Intense Template Name: Effeckt Photo
*/
?>

<figure class="effeckt-caption preload" data-effeckt-type="effeckt-caption-<?php echo esc_attr( $effeckt ); ?>">
	<?php echo intense_get_template( '/gallery/standard/' . $standard_template . '.php', array( 
                'width' => $width,
                'height' => $height,
                'radius' => $radius,
                'url' => $url,
                'title' => $title,
                'description' => $description,
                'style' => 'width: 100%;',
                'lazy_load' => $lazy_load,
                'animation' => $animation
    ) ); ?>
	<figcaption style="<?php echo esc_attr( $effeckt_style ); ?>">
		<div class="effeckt-figcaption-wrap">
			<h3><?php echo esc_html( $title ); ?></h3>
			<p><?php echo esc_html( $description ); ?></p>
		</div>
	</figcaption>
</figure>