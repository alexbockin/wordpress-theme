<?php
/*
Intense Template Name: Subtle Photo
*/

$split_title = explode(' ', $title, 3);
?>

<div class="intense subtle-effect">
	<figure class="intense effect-<?php echo esc_attr( $subtleeffect ); ?>" style="<?php echo ( !empty( $subtleheight ) ? esc_attr( $subtleheight ) : '' ); ?>">
		<?php echo intense_get_template( '/gallery/standard/' . $standard_template . '.php', array( 
                'width' => $width,
                'height' => $height,
                'radius' => $radius,
                'url' => $url,
                'title' => $title,
                'description' => $description,
                'class' => 'subtle-effect-img',
                'lazy_load' => $lazy_load,
                'animation' => $animation
    	) ); ?>
        <figcaption class="subtle-effect-caption">
        	<h2><?php echo ( !empty( $split_title[0] ) ? esc_html( $split_title[0] ) : '' ) . "<span>" . ( !empty( $split_title[1] ) ? ' ' . esc_html( $split_title[1] ) : '' ) . "</span>" . ( !empty( $split_title[2] ) ? ' ' . esc_html( $split_title[2] ) : '' ); ?></h2>
        	<p><?php echo esc_html( $description ); ?></p>
        </figcaption>
    </figure>
</div>