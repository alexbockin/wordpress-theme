<?php
/*
Intense Template Name: Galleria Photo
*/
?>

<img
	src="<?php echo esc_url( $url ); ?>"
	data-big="<?php echo esc_url( $original_url ); ?>"
	data-title="<?php echo esc_attr( $title ); ?>"
	data-description="<?php echo esc_attr( $description ); ?>"	
	style="display: none;" 
/>