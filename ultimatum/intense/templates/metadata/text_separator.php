<?php
/*
Intense Template Name: Text with Separator
*/

$intense_metadata = Intense_Metadata::get_metadata();

Intense()->add_dynamic_css( 'text-metadata-separator', "
	.intense.metadata > span:before {
		content: ' | ';
		padding: 0 5px;
	}

	.intense.metadata > span:first-of-type:before {
		content: '';
		padding: 0;
	}
");

?>
<div class="intense metadata entry-meta<?php echo ( is_rtl() ? ' rtl' : '' ); ?>">			
	<?php if ( $intense_metadata['show_date'] ) : ?>
		<span>		
			<a href="<?php echo esc_url( $intense_metadata['permalink'] ); ?>" title="<?php echo esc_attr( $intense_metadata['time'] ); ?>" rel="bookmark">
				<time class="entry-date" datetime="<?php echo esc_attr( $intense_metadata['datetime'] ); ?>">
					<?php echo esc_html( $intense_metadata['date'] ); ?>
				</time>
			</a>
		</span>
	<?php endif; ?>

	<?php if ( $intense_metadata['show_author'] && !is_author() ) : ?>
	<span class="author vcard">
		<span class="byline"><?php esc_html_e( " by ", "intense" ); ?></span>	
		<a class="url fn n" href="<?php echo esc_url( $intense_metadata['author_url'] ); ?>" title="<?php echo __( "View all posts by", "intense" ) . " " . esc_attr( $intense_metadata['author'] ); ?>" rel="author"><?php echo esc_html( $intense_metadata['author'] ); ?></a>
	</span>
	<?php endif; ?>
	
	<?php if ( $intense_metadata['show_categories'] && count( $intense_metadata['categories'] ) ) : $categories_list = get_the_category_list( ', ', null, $intense_metadata['post_id'] );?>	
	
	<span><?php echo get_the_category_list( __( ' • ', 'intense' ) ); ?></span>
	
	<?php endif; ?>

	<?php if ( $intense_metadata['show_tags'] && count( $intense_metadata['tags'] ) ) : $tags_list = get_the_tag_list( '', ', ', '', $intense_metadata['post_id'] );?>	
	<span>
		<?php echo $tags_list; ?>
	</span>
	<?php endif; ?>
	
	<?php if ( $intense_metadata['show_comments'] ) : ?>
	<span>		
		<a href="<?php echo esc_url( $intense_metadata['permalink'] ); ?>#comments"><?php echo esc_html( $intense_metadata['comment_count'] ); ?> <?php echo esc_html( $intense_metadata['comment_text'] ); ?></a>
	</span>
	<?php endif; ?>
</div>
