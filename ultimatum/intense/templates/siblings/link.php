<?php
/*
Intense Template Name: Link
*/

$intense_active_page = Intense_List_Pages_Walker::get_metadata();

echo ' <a href="' . esc_url( get_permalink( $intense_active_page->ID ) ) . '">'  . esc_html( $intense_active_page->post_title ) .'</a>';
