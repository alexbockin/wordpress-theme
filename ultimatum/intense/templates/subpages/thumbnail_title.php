<?php
/*
Intense Template Name: Thumbnail with Title
*/

$intense_active_page = Intense_List_Pages_Walker::get_metadata();

echo ' <a href="' . esc_url( get_permalink( $intense_active_page->ID ) ) . '" title="' . esc_attr( $intense_active_page->post_title ) . '">'  . intense_get_post_thumbnails( 
	"square75", 
	null, 
	false, 
	true, 
	null, 
	null, 
	null, 
	null,
	"",
	true, 
	true ) 
.'<br>' 
. esc_html( $intense_active_page->post_title )
.'</a>';
