<?php
/*
Intense Template Name: One Column (text right)
*/

$post = get_post(); 
$intense_custom_post = Intense_Custom_Post::get_metadata();

$intense_post_type = $intense_custom_post['post_type'];
?>
<article class='intense row <?php echo esc_attr( $intense_custom_post['post_classes'] ); ?> intense_post nogutter' style='<?php echo esc_attr( $intense_custom_post['plugin_layout_style'] ); ?>' id='post-<?php echo $post->ID; ?>'>
	<?php echo $intense_custom_post['animation_wrapper_start']; ?>

	<div class="intense col-lg-7 col-md-7 col-sm-7 col-xs-7">

		<?php echo intense_get_template( '/custom-post/shared/post_media.php', array( 'size' => intense_coalesce( $intense_custom_post['image_size'], 'medium800' ) ) ); ?>
		
	</div>	
	<div class="intense col-lg-5 col-md-5 col-sm-5 col-xs-5">
		<!-- Head -->
		<div class='post-header'>
			<?php echo intense_get_template( '/custom-post/shared/post_title.php', array( 'tag' => 'h2' ) ); ?>

			<?php echo intense_get_template( '/custom-post/shared/post_subtitle.php', array( 'tag' => 'h4', 'margin' => '10px 0 0 0' ) ); ?>
			
			<?php
				if ( get_field( 'intense_event_start_date' ) != '' ) {
					$startdate = get_field( 'intense_event_start_date' );
					$enddate = '';
					$startdate = date("M d, Y", strtotime( $startdate ) );

					if ( get_field( 'intense_event_end_date' ) != '' && get_field( 'intense_event_end_date' ) != get_field( 'intense_event_start_date' ) ) {
						$enddate = ' - ' .  date( "M d, Y", strtotime( get_field( 'intense_event_end_date' ) ) );
					}

					echo '<h5 style="margin:10px 0;">' . $startdate . $enddate . '</h5>';
				}
			?>
		</div>

		<!-- Content -->	
		<div class='entry-content'>
			<?php echo intense_template_content( $intense_post_type, $intense_custom_post['template_content'], $intense_custom_post['template_content_length'], 60 ); ?>
		</div>
		<?php if ( get_field( 'intense_event_buy_link' ) != '' ) { ?>
			<span>
			<?php echo intense_run_shortcode( 'intense_button', array( 'size' => 'mini', 'color' => 'primary', 'link' => get_field( 'intense_event_buy_link' ), 'icon' => 'angle-right', 'icon_position' => 'right' ), 'Buy Tickets Now' ); ?>
			</span>
		<?php } ?>
		<div class="clearfix"></div>

		<!-- Footer -->
		<footer style='padding-top: 5px;'>
			<?php if ( $intense_custom_post['show_social_sharing'] ) { ?>
				<span class='pull-left'>
					
					<?php echo intense_get_template( '/custom-post/shared/post_social_sharing.php' ); ?>
					
				</span>
			<?php } ?>
		</footer>
	</div>	

	<?php echo $intense_custom_post['animation_wrapper_end']; ?>
</article>

<?php echo intense_run_shortcode( 'intense_spacer', array( 'height' => '15' ) ); ?>
