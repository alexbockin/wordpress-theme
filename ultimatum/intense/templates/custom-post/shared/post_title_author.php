<?php
/*
Intense Template Name: Post Title
*/
$intense_custom_post = Intense_Custom_Post::get_metadata();
?>

<<?php echo $tag; ?> class="entry-title" <?php echo ( !empty( $margin ) ? 'style="margin: ' . esc_attr( $margin ) . ';"' : '' ); ?>>
	<?php if ( $intense_custom_post['show_author'] && !is_author() ) { 
		$author_bio = intense_get_author_bio( get_the_author_meta( 'ID' ), 'one_image', 'square75' );		
	?>
	<a style="padding: 0 20px 20px 0;" href="<?php echo esc_url( get_author_posts_url( get_the_author_meta( 'ID' ) ) ); ?>" rel="author"><img width="50" height="50" src="<?php echo esc_url( $author_bio['image'] ); ?>"/></a>
	<?php } ?>
	<?php if ( !empty( $intense_custom_post['template_link_url'] ) ) { ?>
	<?php echo '<a href="' . esc_url( $intense_custom_post['template_link_url'] ) . '" title="' . the_title_attribute( 'echo=0' ) . '" rel="bookmark">'; ?>
	<?php } ?>
		<?php echo the_title_attribute( 'echo=0' )?>
	<?php if ( !empty( $intense_custom_post['template_link_url'] ) ) { ?>
		</a>
	<?php } ?>
	<?php echo $intense_custom_post['edit_link']; ?>
</<?php echo $tag; ?>>