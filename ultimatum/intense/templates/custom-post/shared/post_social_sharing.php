<?php
/*
Intense Template Name: Post Social Sharing
*/

$intense_custom_post = Intense_Custom_Post::get_metadata(); 

if ( empty( $position ) ) $position = 'left';

if ( $intense_custom_post['show_social_sharing'] ) { ?>
	<span class="pull-<?php echo esc_attr( $position ); ?>">
		<?php 
			$output_shortcode = '';
			$output_shortcode .= '[intense_social_share share_url="' . esc_url( $intense_custom_post['template_link_url'] )  . '" ';
			$output_shortcode .= ( Intense()->options['intense_social_facebook_use'] == 1 ? 'show_facebook="' . Intense()->options['intense_social_facebook_use'] . '" facebook_button="' . Intense()->options['intense_social_facebook_layout'] . '" facebook_faces="' . Intense()->options['intense_social_facebook_faces'] . '" facebook_share="' . Intense()->options['intense_social_facebook_share'] . '" ' : '' );
			$output_shortcode .= ( Intense()->options['intense_social_google_plus_use'] == 1 ? 'show_googleplus="' . Intense()->options['intense_social_google_plus_use'] . '" googleplus_button="' . Intense()->options['intense_social_google_plus_layout'] . '" ' : '' );
			$output_shortcode .= ( Intense()->options['intense_social_linkedin_use'] == 1 ? 'show_linkedin="' . Intense()->options['intense_social_linkedin_use'] . '" linkedin_button="' . Intense()->options['intense_social_linkedin_layout'] . '" ' : '' );
			$output_shortcode .= ( Intense()->options['intense_social_stumbleupon_use'] == 1 ? 'show_stumbleupon="' . Intense()->options['intense_social_stumbleupon_use'] . '" stumbleupon_button="' . Intense()->options['intense_social_stumbleupon_layout'] . '" ' : '' );
			$output_shortcode .= ( Intense()->options['intense_social_twitter_use'] == 1 ? 'show_twitter="' . Intense()->options['intense_social_twitter_use'] . '" twitter_button="' . Intense()->options['intense_social_twitter_layout'] . '" ' : '' );
			$output_shortcode .= ( Intense()->options['intense_social_pinterest_use'] == 1 ? 'show_pinterest="' . Intense()->options['intense_social_pinterest_use'] . '" pinterest_image="' . $intense_custom_post['template_link_url'] . '" pinterest_button="' . Intense()->options['intense_social_pinterest_layout'] . '"' : '' );
			$output_shortcode .= '][/intense_social_share]';
			echo do_shortcode( $output_shortcode );
		 ?>
	</span>
	<?php if ( $position == 'right' ): ?>
	<div class="clearfix"></div>
	<?php endif; ?>
<?php }