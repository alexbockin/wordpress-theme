<?php
/*
Intense Template Name: Two Columns (text on left)
*/

$intense_custom_post = Intense_Custom_Post::get_metadata();

$intense_custom_post['show_meta'] = 0;

echo intense_get_template( '/custom-post/post/two_text_left.php', array( 'after_title' => intense_get_template( '/custom-post/intense_team/shared/member_title.php' ) ) );