<?php
/*
Intense Template Name: Product Price
*/

$product = wc_get_product();
$post = get_post(); 
$intense_custom_post = Intense_Custom_Post::get_metadata();

$intense_post_type = $intense_custom_post['post_type'];

if ( ! $product || ! $product->is_visible() ) {
	return;
}

if ( $price_html = $product->get_price_html() ) {
	echo '<span class="price">' . $price_html . '</span>';
}
