<?php
/*
Intense Template Name: Product Sale Ribbon on Left
*/

$product = wc_get_product();
$post = get_post(); 
$intense_custom_post = Intense_Custom_Post::get_metadata();

$intense_post_type = $intense_custom_post['post_type'];

if ( ! $product || ! $product->is_visible() ) {
	return;
}

if ( $product->is_on_sale() ) {
	echo apply_filters( 'woocommerce_sale_flash', '<span class="ribbon-wrapper left"><span class="onsale ribbon left">' . __( 'Sale!', 'woocommerce' ) . '</span></span>', $post, $product );
}
