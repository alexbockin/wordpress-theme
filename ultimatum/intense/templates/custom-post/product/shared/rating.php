<?php
/*
Intense Template Name: Product Price
*/

$product = wc_get_product();
$post = get_post(); 
$intense_custom_post = Intense_Custom_Post::get_metadata();

$intense_post_type = $intense_custom_post['post_type'];

if ( !isset( $rating_style ) ) $rating_style = '';

if ( ! $product || ! $product->is_visible() ) {
	return;
}

if ( get_option( 'woocommerce_enable_review_rating' ) !== 'no' && $rating_html = $product->get_rating_html() ) {
	echo '<div style="' . $rating_style . '">' . $rating_html . '</div>';
}
