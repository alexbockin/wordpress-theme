<?php
/*
Intense Template Name: Two Columns (text inside)
*/

$post = get_post(); 
$intense_custom_post = Intense_Custom_Post::get_metadata();

if ( $intense_custom_post['index'] % 2 == 0 ) {
	echo intense_get_template( '/custom-post/post/two_text_right.php' );
} else {
	echo intense_get_template( '/custom-post/post/two_text_left.php' );
}
