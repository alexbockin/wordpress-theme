<?php
/*
Intense Template Name: Three Columns (no text)
*/

$post = get_post(); 
$intense_custom_post = Intense_Custom_Post::get_metadata();

$inline = ( is_sticky() && 'inline' == $intense_custom_post['sticky_mode'] );

if ( $inline ) {
	$span = "12";
	$size = "postWide";
} else {
	$span = "4";
	$size = ( isset( $intense_custom_post['image_size'] ) ? $intense_custom_post['image_size'] : 'medium640' );
}

$gutter = ( isset( $intense_custom_post['show_gutter'] ) && $intense_custom_post['show_gutter'] ? 'gutter' : 'nogutter' );
?>
<div class='intense col-lg-<?php echo esc_attr( $span ); ?> col-md-<?php echo esc_attr( $span ); ?> col-sm-12 col-xs-12 <?php echo esc_attr( $intense_custom_post['post_classes'] ); ?> intense_post <?php echo $gutter; ?>' style='display:inline-block; vertical-align: top;'>
	<?php echo $intense_custom_post['animation_wrapper_start']; ?>
	<article id='post-<?php echo $post->ID; ?>' class='<?php echo ( esc_attr( $inline ) ?  'featured ' : '' ); ?>'>

		<?php echo intense_get_template( '/custom-post/shared/post_media.php', array( 'size' => $size, 'custom_post_image_field' => ( isset( $custom_post_image_field ) ? $custom_post_image_field : null ) ) ); ?>

	</article>
	<?php echo $intense_custom_post['animation_wrapper_end']; ?>
</div>