<?php
/*
Intense Template Name: Two Columns (text on left)
*/

$post = get_post(); 
$intense_custom_post = Intense_Custom_Post::get_metadata();

$inline = ( is_sticky() && 'inline' == $intense_custom_post['sticky_mode'] );

if ( $inline ) {
	$span = "12";
	$size = "postWide";
	$header_tag = "h2";
} else {
	$span = "6";
	$size = ( isset( $intense_custom_post['image_size'] ) ? $intense_custom_post['image_size'] : 'medium640' );
	$header_tag = "h2";
}

$padding_bottom = Intense()->options['intense_layout_row_default_padding']['padding-top'];
$intense_post_type = $intense_custom_post['post_type'];
?>
<div class='intense col-lg-<?php echo esc_attr( $span ); ?> col-md-<?php echo esc_attr( $span ); ?> col-sm-12 col-xs-12 <?php echo esc_attr( $intense_custom_post['post_classes'] ); ?> intense_post nogutter' style='margin-left: 0px; float: none; padding: 0 10px; padding-bottom: <?php echo esc_attr( $padding_bottom ); ?>; display:inline-block; vertical-align: top;'>
	<article id='post-<?php echo $post->ID; ?>' class='<?php echo ( esc_attr( $inline ) ?  'featured ' : '' ); ?>'>
	<?php echo $intense_custom_post['animation_wrapper_start']; ?>
		<div class="intense row">			
			<div class="intense col-md-6 col-lg-6 col-sm-12 col-xs-12">
				<?php 
				// Set from other templates (ex. books)
				if ( isset( $after_media_content ) ) echo $after_media_content; 
				?>
				
				<?php echo intense_get_template( '/custom-post/shared/post_title.php', array( 'tag' => $header_tag ) ); ?>

				<?php echo intense_get_template( '/custom-post/shared/post_subtitle.php', array( 'tag' => 'h4' ) ); ?>

				<?php 
				// Set from other templates (ex. team)
				if ( isset( $after_title ) ) echo $after_title; 
				?>
				
				<?php echo intense_get_template( '/custom-post/shared/post_metadata.php' ); ?>

				<div class='entry-content'>
					<?php echo intense_template_content( $intense_post_type, $intense_custom_post['template_content'], $intense_custom_post['template_content_length'], 20 ); ?>
				</div>
				
				<footer>
					
					<?php echo intense_get_template( '/custom-post/shared/post_social_sharing.php' ); ?>

					<?php echo intense_get_template( '/custom-post/shared/post_read_more.php' ); ?>

					<?php echo intense_run_shortcode( 'intense_spacer', array( 'height' => '15' ) ); ?>
					
				</footer>
			</div>
			<div class="intense col-md-5 col-lg-5 col-sm-12 col-xs-12">
				<?php echo intense_get_template( '/custom-post/shared/post_media.php', array( 'size' => $size, 'custom_post_image_field' => ( isset( $custom_post_image_field ) ? $custom_post_image_field : null ) ) ); ?>
			</div>
		</div>
	<?php echo $intense_custom_post['animation_wrapper_end']; ?>
	</article>
	<?php echo intense_run_shortcode( 'intense_spacer', array( 'height' => '30' ) ); ?>
</div>