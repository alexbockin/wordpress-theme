<?php
/*
Intense Template Name: Three Columns (text)
*/

$post = get_post(); 
$intense_custom_post = Intense_Custom_Post::get_metadata();

$inline = ( is_sticky() && 'inline' == $intense_custom_post['sticky_mode'] );

if ( $inline ) {
	$span = "12";
	$size = "postWide";
	$header_tag = "h2";
} else {
	$span = "4";
	$size = ( isset( $intense_custom_post['image_size'] ) ? $intense_custom_post['image_size'] : 'medium640' );
	$header_tag = "h3";
}

$padding_bottom = Intense()->options['intense_layout_row_default_padding']['padding-top'];
$intense_post_type = $intense_custom_post['post_type'];
?>
<div class='intense col-lg-<?php echo esc_attr( $span ); ?> col-md-<?php echo esc_attr( $span ); ?> col-sm-12 col-xs-12 <?php echo esc_attr( $intense_custom_post['post_classes'] ); ?> intense_post' style='margin-left: 0px; float: none; padding: 0 10px; padding-bottom: <?php echo esc_attr( $padding_bottom ); ?>; display:inline-block; vertical-align: top;'>
	<?php echo $intense_custom_post['animation_wrapper_start']; ?>
	<article id='post-<?php echo $post->ID; ?>' class='<?php echo ( esc_attr( $inline ) ?  'featured ' : '' ); ?>'>
		
		<?php echo intense_get_template( '/custom-post/shared/post_media.php', array( 'size' => $size, 'custom_post_image_field' => ( isset( $custom_post_image_field ) ? $custom_post_image_field : null ) ) ); ?>

		<?php 
		// Set from other templates (ex. books)
		if ( isset( $after_media_content ) ) echo $after_media_content; 
		?>
		
		<?php echo intense_get_template( '/custom-post/shared/post_title.php', array( 'tag' => $header_tag ) ); ?>

		<?php 
		// Set from other templates (ex. team)
		if ( isset( $after_title ) ) echo $after_title; 
		?>
		
		<?php echo intense_get_template( '/custom-post/shared/post_metadata.php' ); ?>

		<div class='entry-content'>
			<?php echo intense_template_content( $intense_post_type, $intense_custom_post['template_content'], $intense_custom_post['template_content_length'], 20 ); ?>
		</div>

		<?php 
		// Set from other templates (ex. team)
		if ( isset( $after_content ) ) echo $after_content; 
		?>
				
		<?php echo intense_get_template( '/custom-post/shared/post_read_more.php' ); ?>

		<?php echo intense_run_shortcode( 'intense_spacer', array( 'height' => '15' ) ); ?>

	</article>
	<?php echo $intense_custom_post['animation_wrapper_end']; ?>
</div>