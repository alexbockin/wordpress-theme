<?php
/*
Intense Template Name: Four Columns (no text)
*/

$post = get_post(); 
$intense_custom_post = Intense_Custom_Post::get_metadata();

$inline = ( is_sticky() && 'inline' == $intense_custom_post['sticky_mode'] );

if ( $inline ) {
	$span = "12";
	$size = "postWide";
} else {
	$span = "3";
	$size = ( isset( $intense_custom_post['image_size'] ) ? $intense_custom_post['image_size'] : 'medium500' );
}
?>

<div class='intense col-lg-<?php echo esc_attr( $span ); ?> col-md-<?php echo esc_attr( $span ); ?> col-sm-6 col-xs-12 <?php echo esc_attr( $intense_custom_post['post_classes'] ); ?> intense_post nogutter' style='display:inline-block; vertical-align: top;'>
	<article id='post-<?php echo $post->ID; ?>' class='<?php echo ( esc_attr( $inline ) ?  'featured ' : '' ); ?>'>
	<?php echo $intense_custom_post['animation_wrapper_start']; ?>
		<div class='image'>
			<?php 			
			$embed = get_field( 'intense_music_link' );
			$video_type = get_field( 'intense_music_video_type' );
			$video = get_field( 'intense_music_video' );

			if ( !empty( $embed ) ) {
				$var = apply_filters('the_content', "[embed]" . $embed . "[/embed]");
				echo $var;
			} else if ( !empty( $video ) && !empty( $video_type ) ) {
				echo intense_run_shortcode( 'intense_video', array(
					'video_type' => $video_type,
					'video_url' => $video,
					'video_size' => ''
				) );
			} else { ?>
				<?php echo intense_get_template( '/custom-post/shared/post_media.php', array( 'size' => $size ) ); ?>
			<?php } ?>
		</div>
	<?php echo $intense_custom_post['animation_wrapper_end']; ?>
	</article>
</div>