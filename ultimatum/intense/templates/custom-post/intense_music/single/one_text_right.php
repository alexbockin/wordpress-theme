<?php
/*
Intense Template Name: One Column (text right)
*/

function ult_intense_musicr()
{
	$post = get_post();
	$intense_custom_post = Intense_Custom_Post::get_metadata();

	$padding_top = Intense()->options['intense_layout_row_default_padding']['padding-top'];
	$padding_bottom = Intense()->options['intense_layout_row_default_padding']['padding-bottom'];
	$margin_top = Intense()->options['intense_layout_row_default_margin']['margin-top'];
	$margin_bottom = Intense()->options['intense_layout_row_default_margin']['margin-bottom'];

	$layout_style = '';

	if (!empty($padding_top)) {
		$layout_style .= 'padding-top: ' . $padding_top . '; ';
	}

	if (!empty($padding_bottom)) {
		$layout_style .= 'padding-bottom: ' . $padding_bottom . '; ';
	}

	if (!empty($margin_top)) {
		$layout_style .= 'margin-top: ' . $margin_top . '; ';
	}

	if (!empty($margin_bottom)) {
		$layout_style .= 'margin-bottom: ' . $margin_bottom . '; ';
	}

	$no_layout_style = "padding-top: 0px; padding-bottom: 0px; margin-top: 0px; margin-bottom: 0px;";

	$intense_post_type = Intense()->post_types->get_post_type('intense_music');

	$intense_custom_post = array(
		'plugin_layout_style' => $layout_style,
		'cancel_plugin_layout_style' => $no_layout_style,
		'post_type' => $intense_post_type,
		'taxonomy' => '',
		'template' => '',
		'categories' => '',
		'exclude_categories' => '',
		'exclude_post_ids' => '',
		'authors' => '',
		'order_by' => '',
		'order' => '',
		'posts_per_page' => '',
		'image_size' => 'Full',
		'image_shadow' => '0',
		'hover_effect_type' => '',
		'hover_effect' => '',
		'subtle_effect' => '',
		'show_all' => '',
		'show_meta' => '0',
		'show_author' => '0',
		'infinite_scroll' => '',
		'show_filter' => '',
		'show_images' => '1',
		'show_missing_image' => '0',
		'show_social_sharing' => '1',
		'show_read_more' => '0',
		'read_more_text' => 'Read More',
		'timeline_mode' => '',
		'timeline_showyear' => '',
		'timeline_readmore' => '',
		'timeline_color' => '',
		'filter_easing' => '',
		'filter_effects' => '',
		'hover_effect_color' => '',
		'hover_effect_opacity' => '',
		'sticky_mode' => '',
		'image_border_radius' => '',
		'timeline_border_radius' => '',
		'animation_type' => '',
		'animation_trigger' => null,
		'animation_scroll_percent' => null,
		'animation_delay' => null,
		'animation_wrapper_start' => null,
		'animation_wrapper_end' => null,
		'is_slider' => ''
	);

	$i = 0;

	do_action('intense_before_main_content');
	while (have_posts()) : the_post();
		$item_classes = '';
		$intense_custom_post['image_shadow'] = '';
		$intense_custom_post['hover_effect'] = '';
		$intense_custom_post['hover_effect_color'] = '';
		$intense_custom_post['hover_effect_opacity'] = '';
		$intense_custom_post['index'] = $i;

		if (has_post_thumbnail()) {
			if (get_field('intense_image_shadow') != '') {
				$intense_custom_post['image_shadow'] = get_field('intense_image_shadow');
			}

			if (get_field('intense_hover_effect') != '') {
				$intense_custom_post['hover_effect'] = get_field('intense_hover_effect');
			}

			if (get_field('intense_effect_color') != '') {
				$intense_custom_post['hover_effect_color'] = get_field('intense_effect_color');
			}

			if (get_field('intense_effect_opacity') != '') {
				$intense_custom_post['hover_effect_opacity'] = get_field('intense_effect_opacity');
			}
		}

		if (current_user_can('manage_options')) {
			$intense_custom_post['edit_link'] = "&nbsp;" . intense_run_shortcode('intense_button', array('size' => 'mini', 'color' => 'inverse', 'class' => 'post-edit-link', 'link' => get_edit_post_link($post->ID), 'title' => __("Edit Post", "intense")), __("Edit", "intense"));
		} else {
			$intense_custom_post['edit_link'] = '';
		}

		$intense_custom_post['post_classes'] = $item_classes;
		?>

		<article
			class='intense col-lg-12 col-md-12 col-sm-12 col-xs-12 <?php echo esc_attr($intense_custom_post['post_classes']); ?> intense_post nogutter'
			style='<?php echo esc_attr($intense_custom_post['plugin_layout_style']); ?>'
			id='post-<?php echo $post->ID; ?>'>
			<!-- Head -->
			<?php echo $intense_custom_post['animation_wrapper_start']; ?>
			<!-- Image -->
			<div class='intense col-lg-5 col-md-5 col-sm-3 col-xs-12'
				 style="margin-left: 0px; margin-right: 10px; padding:0;">
				<?php
				$image = get_field('intense_music_image');
				$embed = get_field('intense_music_link');
				$audio = get_field('intense_music_audio_clip');

				if (!empty($embed)) {
					$var = apply_filters('the_content', "[embed]" . $embed . "[/embed]");
					echo $var;
				} else if (!empty($image)) { ?>
					<a href='<?php echo get_permalink(); ?>'
					   title='<?php esc_html_e("Permalink to", "intense"); ?>  <?php echo the_title_attribute('echo=0'); ?>'
					   rel='bookmark'>
						<img src='<?php echo esc_url($image["url"]); ?>'
							 title='<?php echo the_title_attribute('echo=0'); ?>' alt='' style='padding:10px 0;'/>
					</a>
					<?php if (!empty($audio)) { ?>
						<div style='padding:5px 0;'>
							<?php echo intense_run_shortcode('intense_audio', array('url' => esc_url($audio))); ?>
						</div>
					<?php } ?>
				<?php } ?>
			</div>

			<?php if (get_field('intense_music_purchase_link') != '' || $intense_custom_post['show_social_sharing']) { ?>
				<div class='pull-right' style='display:inline-block; text-align:middle;'>
					<?php if (get_field('intense_music_video') != '') {
						$random = rand();
						$video_type = get_field('intense_music_video_type');
						$video_url = get_field('intense_music_video');

						$video = intense_run_shortcode('intense_video', array(
							'video_type' => $video_type,
							'video_url' => esc_url($video_url)
						));

						$icon = intense_run_shortcode('intense_icon', array(
							'source' => Intense()->options['intense_play_icon']['source'],
							'type' => Intense()->options['intense_play_icon']['type'],
							'width' => '21',
							'height' => '21',
							'color' => 'primary'
						));

						echo intense_run_shortcode('intense_lightbox', array(
							'type' => 'magnificpopup',
							'content_type' => 'html',
							'html_element' => 'video_trailer_' . $random,
						), $icon);

						echo '<div id="video_trailer_' . $random . '" style="width:800px; height:450px;">' . $video . '</div>';
					}

					if ($intense_custom_post['show_social_sharing']) {
						$output_shortcode = '';
						$output_shortcode .= '[intense_social_share share_url="' . get_permalink($post->ID) . '" size="18" ';
						$output_shortcode .= (Intense()->options['intense_social_facebook_use'] == 1 ? 'show_facebook="' . Intense()->options['intense_social_facebook_use'] . '" facebook_button="' . Intense()->options['intense_social_facebook_layout'] . '" facebook_faces="' . Intense()->options['intense_social_facebook_faces'] . '" facebook_share="' . Intense()->options['intense_social_facebook_share'] . '" ' : '');
						$output_shortcode .= (Intense()->options['intense_social_google_plus_use'] == 1 ? 'show_googleplus="' . Intense()->options['intense_social_google_plus_use'] . '" googleplus_button="' . Intense()->options['intense_social_google_plus_layout'] . '" ' : '');
						$output_shortcode .= (Intense()->options['intense_social_linkedin_use'] == 1 ? 'show_linkedin="' . Intense()->options['intense_social_linkedin_use'] . '" linkedin_button="' . Intense()->options['intense_social_linkedin_layout'] . '" ' : '');
						$output_shortcode .= (Intense()->options['intense_social_stumbleupon_use'] == 1 ? 'show_stumbleupon="' . Intense()->options['intense_social_stumbleupon_use'] . '" stumbleupon_button="' . Intense()->options['intense_social_stumbleupon_layout'] . '" ' : '');
						$output_shortcode .= (Intense()->options['intense_social_twitter_use'] == 1 ? 'show_twitter="' . Intense()->options['intense_social_twitter_use'] . '" twitter_button="' . Intense()->options['intense_social_twitter_layout'] . '" ' : '');
						$output_shortcode .= (Intense()->options['intense_social_pinterest_use'] == 1 ? 'show_pinterest="' . Intense()->options['intense_social_pinterest_use'] . '" pinterest_image="' . get_permalink($post->ID) . '" pinterest_button="' . Intense()->options['intense_social_pinterest_layout'] . '"' : '');
						$output_shortcode .= '[/intense_social_share] ';
						echo do_shortcode($output_shortcode);
					}

					if (get_field('intense_music_purchase_link') != '') {
						$buy = intense_coalesce(get_field('intense_music_purchase_text'), __('Buy', 'intense'));
						echo intense_run_shortcode('intense_button', array('size' => 'small', 'link' => esc_url(get_field('intense_music_purchase_link')), 'target' => '_blank', 'icon' => 'angle-right', 'icon_position' => 'right'), $buy);
					} ?>
				</div>
			<?php } ?>

			<!-- Head -->
			<div class='post-header'>

				<?php echo intense_get_template('/custom-post/shared/post_title.php', array('tag' => 'h2', 'margin' => '5px 0 0 0')); ?>

				<?php
				if (is_object($intense_post_type) && $intense_post_type->get_subtitle() != '') {
					echo '<h4 class="intense music" style="margin: 5px 0;">' . $intense_post_type->get_subtitle() . '</h4>';
				}
				?>
			</div>
			<?php if ($intense_custom_post['show_meta']) {
				echo intense_run_shortcode('intense_metadata', array(
					'type' => 'template',
					'template' => $intense_custom_post['meta_template'],
					'show_comments' => $intense_custom_post['meta_show_comments'],
					'show_author' => $intense_custom_post['meta_show_author'],
					'show_categories' => $intense_custom_post['meta_show_categories'],
					'show_tags' => $intense_custom_post['meta_show_tags'],
					'show_date' => $intense_custom_post['meta_show_date'],
					'date_format' => $intense_custom_post['meta_date_format'],
				));
			} ?>

			<?php
			$artists = get_the_term_list($post->ID, 'intense_music_artist', '', ', ', '');
			$record_labels = get_the_term_list($post->ID, 'intense_music_label', '', ', ', '');
			$catalog = get_the_term_list($post->ID, 'intense_music_catalog', '', ', ', '');
			$release = get_field('intense_music_release_date');
			?>

			<?php if (!empty($artists) || !empty($release) || !empty($record_labels) || !empty($catalog)) { ?>
				<div class='intense music' style='padding-top: 10px;'>
					<?php if (!empty($artists)) { ?>
						<label style="display:block; font-weight: normal;"><strong
								style="text-transform:uppercase;"><?php echo __('Artist(s)', 'intense') ?>
								:</strong> <?php echo $artists; ?></label>
					<?php } ?>
					<?php if (!empty($release)) { ?>
						<label style="display:block; font-weight: normal;"><strong
								style="text-transform:uppercase;"><?php echo __('Release Date', 'intense') ?>
								:</strong> <?php echo date("M d, Y", strtotime($release)); ?></label>
					<?php } ?>
					<?php if (!empty($record_labels)) { ?>
						<label style="display:block; font-weight: normal;"><strong
								style="text-transform:uppercase;"><?php echo __('Label(s)', 'intense') ?>
								: </strong><?php echo $record_labels; ?></label>
					<?php } ?>
					<?php if (!empty($catalog)) { ?>
						<label style="display:block; font-weight: normal;"><strong
								style="text-transform:uppercase;"><?php echo __('Catalog', 'intense') ?>
								: </strong><?php echo $catalog; ?></label>
					<?php } ?>
				</div>
			<?php } ?>
			<div class="clearfix"></div>

			<div class='intense row nogutter'
				 style='<?php echo esc_attr($intense_custom_post['cancel_plugin_layout_style']); ?> padding-top: 10px;'>
				<!-- Content -->
				<div class='entry-content intense col-lg-12 col-md-12 col-sm-12 col-xs-12'>
					<?php
					$content = intense_template_content('intense_music', 'full_post', '', 100);
					echo $content;
					?>
				</div>
			</div>

			<div class='intense row nogutter'
				 style='<?php echo esc_attr($intense_custom_post['cancel_plugin_layout_style']); ?> padding-top: 10px;'>
				<div class='entry-content intense col-lg-12 col-md-12 col-sm-12 col-xs-12'>
					<?php
					$rows = get_field('intense_music_tracks');
					$counter = 1;
					$even_color = '#ddd';
					$odd_color = '#eee';
					$loop_disc_title = '';

					if ($rows) { ?>
						<?php foreach ($rows as $row) {
							$title = $row['intense_music_track_title'];
							$track_title = $title;
							$track_artists = $row['intense_music_track_artists'];
							$track_albums = $row['intense_music_track_album'];
							$track_genres = $row['intense_music_track_genre'];
							$track_remixers = $row['intense_music_track_remixers'];
							$track_disc_title = $row['intense_music_track_disc_title'];
							$length = $row['intense_music_track_title'];
							$bpm = $row['intense_music_track_bpm'];
							$key = $row['intense_music_track_key'];
							$length = $row['intense_music_track_length'];
							$track_lyrics = $row['intense_music_track_lyrics'];
							$track_embed = $row['intense_music_track_link'];
							$track_audio = $row['intense_music_track_audio_clip'];
							$track_audio_lb = '';
							$track_embed_lb = '';
							$track_lyrics_lb = '';
							$artist_list = '';
							$genre_list = '';
							$album_list = '';
							$remixer_list = '';
							$track_random = rand();
							$lightbox_link = '';

							if ($track_artists) {
								$artist_list = intense_get_taxonomy_list($track_artists, 'name');
							}

							if ($track_albums) {
								$album_list = intense_get_taxonomy_list($track_albums, 'name');
							}

							if ($track_genres) {
								$genre_list = intense_get_taxonomy_list($track_genres, 'name');
							}

							if ($track_remixers) {
								$remixer_list = intense_get_taxonomy_list($track_remixers, 'name');
							}

							if ($counter % 2 == 0) {
								$background_color = $even_color;
							} else {
								$background_color = $odd_color;
							}

							if (($loop_disc_title == '' && $counter == 1) || $loop_disc_title != $track_disc_title) { ?>
								<?php $background_color = $odd_color; ?>
								<h3><?php echo $track_disc_title . ' ' . __('Tracks', 'intense') ?>:</h3>

								<div class="intense row"
									 style="font-size:13px; background-color:<?php echo $background_color; ?>; border-bottom:1px solid rgba(0,0,0,0.1); padding:5px 0;">
									<div class="intense col-lg-3 col-md-3 col-sm-9 col-xs-12">
										<strong><?php echo __('Title', 'intense') ?></strong>
									</div>
									<div class="intense col-lg-2 col-md-2 hidden-sm hidden-xs">
										<strong><?php echo __('Artists', 'intense') ?></strong>
									</div>
									<div class="intense col-lg-2 col-md-2 hidden-sm hidden-xs">
										<strong><?php echo __('Genre', 'intense') ?></strong>
									</div>
									<div class="intense col-lg-2 col-md-2 hidden-sm hidden-xs">
										<strong><?php echo __('Remixers', 'intense') ?></strong>
									</div>
									<div class="intense col-lg-1 col-md-1 hidden-sm hidden-xs">
										<strong><?php echo __('BPM', 'intense') ?></strong>
									</div>
									<div class="intense col-lg-1 col-md-1 hidden-sm hidden-xs">
										<strong><?php echo __('Key', 'intense') ?></strong>
									</div>
									<div class="intense col-lg-1 col-md-1 col-sm-3 hidden-xs">
										<strong><?php echo __('Length', 'intense') ?></strong>
									</div>
								</div>
								<?php $counter = 1; ?>
							<?php }

							if (!empty($track_embed)) {
								$track_embed_lb = apply_filters('the_content', "[embed]" . $track_embed . "[/embed]");
							}

							if (!empty($track_audio)) {
								$track_audio_lb = "<div style='padding:5px 0;'>" . intense_run_shortcode('intense_audio', array('url' => esc_url($track_audio))) . "</div>";
							}

							if (!empty($track_lyrics)) {
								$track_lyrics_lb = "<div style='width:100%; height:100%; background-color:" . esc_attr($background_color) . "; padding:10px; overflow-y:scroll;'><div style='width:100%; height:30px;'></div><h3>" . $track_title . '</h3>' . $track_lyrics . "</div>";
							}

							if (!empty($track_embed_lb) || !empty($track_audio_lb) || !empty($track_lyrics_lb)) {
								$title = intense_run_shortcode('intense_lightbox', array(
									'type' => 'magnificpopup',
									'content_type' => 'html',
									'html_element' => 'track_info_' . $track_random,
								), $title);
							}
							?>

							<div class="intense row"
								 style="font-size:12px; background-color:<?php echo $background_color; ?>; padding:3px 0;">
								<div class="intense col-lg-3 col-md-3 col-sm-9 col-xs-12">
									<?php echo '<strong>' . $counter . '.</strong>  ' . $title ?>
								</div>
								<div class="intense col-lg-2 col-md-2 hidden-sm hidden-xs">
									<?php echo $artist_list ?>
								</div>
								<div class="intense col-lg-2 col-md-2 hidden-sm hidden-xs">
									<?php echo $genre_list ?>
								</div>
								<div class="intense col-lg-2 col-md-2 hidden-sm hidden-xs">
									<?php echo $remixer_list ?>
								</div>
								<div class="intense col-lg-1 col-md-1 hidden-sm hidden-xs">
									<?php echo $bpm ?>
								</div>
								<div class="intense col-lg-1 col-md-1 hidden-sm hidden-xs">
									<?php echo $key ?>
								</div>
								<div class="intense col-lg-1 col-md-1 col-sm-3 hidden-xs">
									<?php echo $length ?>
								</div>
							</div>

							<?php

							if (!empty($track_embed_lb) || !empty($track_audio_lb) || !empty($track_lyrics_lb)) {
								echo '<div id="track_info_' . $track_random . '" style="width:800px; height:600px;">' . $track_embed_lb . $track_audio_lb . $track_lyrics_lb . '</div>';
							} ?>

							<?php $loop_disc_title = $track_disc_title; ?>
							<?php $counter++; ?>
						<?php } ?>
					<?php } ?>
				</div>
			</div>
			<?php echo $intense_custom_post['animation_wrapper_end']; ?>
		</article>

		<?php
		$i++;
	endwhile;

	do_action('intense_after_main_content');
}
remove_action( 'ultimatum_loop', 'ultimatum_standard_loop' );
add_action('ultimatum_loop', 'ult_intense_musicr');
ultimatum();
