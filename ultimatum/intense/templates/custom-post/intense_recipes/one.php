<?php
/*
Intense Template Name: One Column
*/

$post = get_post(); 
$intense_custom_post = Intense_Custom_Post::get_metadata();

$intense_post_type = $intense_custom_post['post_type'];
?>

<article class='intense col-lg-12 col-md-12 col-sm-12 col-xs-12 <?php echo esc_attr( $intense_custom_post['post_classes'] ); ?> intense_post nogutter' style='<?php echo esc_attr( $intense_custom_post['plugin_layout_style'] ); ?>' id='post-<?php echo $post->ID; ?>'>
	<!-- Head -->
	<?php echo $intense_custom_post['animation_wrapper_start']; ?>
		
	<?php if ( $intense_custom_post['show_images'] ) { ?>
	<!-- Image -->
	<div class='intense row' style='<?php echo esc_attr( $intense_custom_post['cancel_plugin_layout_style'] ); ?>'>
		<div class='intense row col-lg-12 col-md-12 col-sm-12 col-xs-12' style='margin:0; margin-left: 15px; padding: 10px 0 0 0; background-color: rgba(0,0,0,0.6); color: #e8e8e8; box-shadow: 0 5px 2px rgba(0,0,0,0.2); position:absolute; z-index:1; text-align: center;'>
			<div class='entry-content intense col-lg-4 col-md-4 col-sm-12 col-xs-12'>
				<label><strong><?php echo __('Prep Time', 'intense' ) ?>:</strong> <?php echo intense_convert_minutes_to_hours( get_field('intense_recipe_prep_time') ); ?></label>
			</div>
			<div class='entry-content intense col-lg-4 col-md-4 col-sm-12 col-xs-12'>
				<label><strong><?php echo __('Cook Time', 'intense' ) ?>: </strong><?php echo intense_convert_minutes_to_hours( get_field('intense_recipe_cook_time') ); ?></label>
			</div>
			<div class='entry-content intense col-lg-4 col-md-4 col-sm-12 col-xs-12'>
				<label><strong><?php echo __('Ready In', 'intense' ) ?>: </strong><?php echo intense_convert_minutes_to_hours( ( get_field('intense_recipe_prep_time') + get_field('intense_recipe_cook_time') ) ); ?></label>
			</div>
		</div>
		<div class='intense col-lg-12 col-md-12 col-sm-12 col-xs-12' style='position: relative;'>
			
			<?php echo intense_get_template( '/custom-post/shared/post_media.php', array( 'size' => intense_coalesce( $intense_custom_post['image_size'], 'postWide' ) ) ); ?>
			
		</div>
	</div>
	<?php } ?>

	<div class='intense row' style='<?php echo esc_attr( $intense_custom_post['cancel_plugin_layout_style'] ); ?>'>
		<div class='intense col-lg-12 col-md-12 col-sm-12 col-xs-12'>
			<div class='post-header'>

				<?php echo intense_get_template( '/custom-post/shared/post_title.php', array( 'tag' => 'h2' ) ); ?>

				<?php echo intense_get_template( '/custom-post/shared/post_metadata.php' ); ?>

			</div>	
		</div>
	</div>

	<!-- Content -->
	<div class='intense row' style='<?php echo esc_attr( $intense_custom_post['cancel_plugin_layout_style'] ); ?> padding-top: 10px;'>
		<div class='entry-content intense col-lg-12 col-md-12 col-sm-12 col-xs-12'>
			<?php echo intense_template_content( $intense_post_type, $intense_custom_post['template_content'], $intense_custom_post['template_content_length'], 100 ); ?>
		</div>
	</div>
	<?php
	$cuisine = get_the_term_list( $post->ID, 'intense_recipes_cuisines', '', ', ', '' );
	$course = get_the_term_list( $post->ID, 'intense_recipes_courses', '', ', ', '' );
	$skill_level = get_the_term_list( $post->ID, 'intense_recipes_skill_levels', '', ', ', '' );
	?>
	<!-- Footer -->
	<footer style='padding: 5px 0 10px 0;'>
		<div class='intense row' style='<?php echo esc_attr( $intense_custom_post['cancel_plugin_layout_style'] ); ?> padding-top: 5px;'>
			<div class='entry-content intense col-lg-4 col-md-4 col-sm-12 col-xs-12'>
				<label><strong><?php echo __('Cuisine', 'intense' ) ?>:</strong> <?php echo $cuisine; ?></label>
			</div>
			<div class='entry-content intense col-lg-4 col-md-4 col-sm-12 col-xs-12'>
				<label><strong><?php echo __('Course', 'intense' ) ?>: </strong><?php echo $course; ?></label>
			</div>
			<div class='entry-content intense col-lg-4 col-md-4 col-sm-12 col-xs-12'>
				<label><strong><?php echo __('Skill Level', 'intense' ) ?>: </strong><?php echo $skill_level; ?></label>
			</div>
		</div>
	</footer>
	<?php echo $intense_custom_post['animation_wrapper_end']; ?>
</article>
