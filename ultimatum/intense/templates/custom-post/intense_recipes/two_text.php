<?php
/*
Intense Template Name: Two Columns (text)
*/

$post = get_post(); 
$intense_custom_post = Intense_Custom_Post::get_metadata();

$inline = ( is_sticky() && 'inline' == $intense_custom_post['sticky_mode'] );

if ( $inline ) {
	$span = "12";
	$size = "postWide";
	$header_tag = "h3";
} else {
	$span = "6";
	$size = ( isset( $intense_custom_post['image_size'] ) ? $intense_custom_post['image_size'] : 'medium800' );
	$header_tag = "h3";
}

$padding_bottom = Intense()->options['intense_layout_row_default_padding']['padding-top'];
$intense_post_type = $intense_custom_post['post_type'];
?>
<div class='intense col-lg-<?php echo esc_attr( $span ); ?> col-md-<?php echo esc_attr( $span ); ?> col-sm-12 col-xs-12 <?php echo esc_attr( $intense_custom_post['post_classes'] ); ?> intense_post nogutter' style='margin-left: 0px; float: none; padding: 0 10px; padding-bottom: <?php echo esc_attr( $padding_bottom ); ?>; display:inline-block; vertical-align: top;'>
	<article id='post-<?php echo $post->ID; ?>' class='<?php echo ( esc_attr( $inline ) ?  'featured ' : '' ); ?>' style='position:relative;'>
	<?php echo $intense_custom_post['animation_wrapper_start']; ?>
		<?php if ( $intense_custom_post['show_images'] ) { ?>
			<div class='intense row' style='<?php echo esc_attr( $intense_custom_post['cancel_plugin_layout_style'] ); ?>'>
				<div class='intense row col-lg-12 col-md-12 col-sm-12 col-xs-12' style='margin:0; margin-left: 15px; padding: 10px 0 0 0; background-color: rgba(0,0,0,0.6); color: #e8e8e8; box-shadow: 0 5px 2px rgba(0,0,0,0.2); position:absolute; z-index:1; text-align: center;'>
					<div class='entry-content intense col-lg-4 col-md-4 col-sm-12 col-xs-12' style='color: #e8e8e8;'>
						<label><strong><?php echo __('Prep', 'intense' ) ?></strong><h4 style='color: #e8e8e8; margin: 5px;'><?php echo intense_convert_minutes_to_hours( get_field('intense_recipe_prep_time') ); ?></h4></label>
					</div>
					<div class='entry-content intense col-lg-4 col-md-4 col-sm-12 col-xs-12' style='color: #e8e8e8;'>
						<label><strong><?php echo __('Cook', 'intense' ) ?></strong><h4 style='color: #e8e8e8; margin: 5px;'><?php echo intense_convert_minutes_to_hours( get_field('intense_recipe_cook_time') ); ?></h4></label>
					</div>
					<div class='entry-content intense col-lg-4 col-md-4 col-sm-12 col-xs-12' style='color: #e8e8e8;'>
						<label><strong><?php echo __('Ready', 'intense' ) ?></strong><h4 style='color: #e8e8e8; margin: 5px;'><?php echo intense_convert_minutes_to_hours( ( get_field('intense_recipe_prep_time') + get_field('intense_recipe_cook_time') ) ); ?></h4></label>
					</div>
				</div>				
				<div class='intense col-lg-12 col-md-12 col-sm-12 col-xs-12' style='position: relative;'>
					
					<?php echo intense_get_template( '/custom-post/shared/post_media.php', array( 'size' => $size ) ); ?>

				</div>
			</div>
		<?php } ?>	

		<?php echo intense_get_template( '/custom-post/shared/post_title.php', array( 'tag' => $header_tag ) ); ?>

		<?php echo intense_get_template( '/custom-post/shared/post_metadata.php' ); ?>

		<div class='entry-content'>
			<?php echo intense_template_content( $intense_post_type, $intense_custom_post['template_content'], $intense_custom_post['template_content_length'], 40 ); ?>

			<?php
			$cuisine = get_the_term_list( $post->ID, 'intense_recipes_cuisines', '', ', ', '' );
			$course = get_the_term_list( $post->ID, 'intense_recipes_courses', '', ', ', '' );
			$skill_level = get_the_term_list( $post->ID, 'intense_recipes_skill_levels', '', ', ', '' );
			?>
			<!-- Footer -->
			<footer style='padding-top: 5px;'>
				<div class='intense row' style='<?php echo esc_attr( $intense_custom_post['cancel_plugin_layout_style'] ); ?> padding-top: 5px;'>
					<div class='entry-content intense col-lg-4 col-md-4 col-sm-12 col-xs-12'>
						<label><strong><?php echo __('Cuisine', 'intense' ) ?>:</strong> <?php echo $cuisine; ?></label>
					</div>
					<div class='entry-content intense col-lg-4 col-md-4 col-sm-12 col-xs-12'>
						<label><strong><?php echo __('Course', 'intense' ) ?>: </strong><?php echo $course; ?></label>
					</div>
					<div class='entry-content intense col-lg-4 col-md-4 col-sm-12 col-xs-12'>
						<label><strong><?php echo __('Skill Level', 'intense' ) ?>: </strong><?php echo $skill_level; ?></label>
					</div>
				</div>
			</footer>
			
			<?php echo intense_get_template( '/custom-post/shared/post_social_sharing.php' ); ?>
			
		</div>
	<?php echo $intense_custom_post['animation_wrapper_end']; ?>
	</article>
</div>