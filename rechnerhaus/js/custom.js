/**
 * Rechnerhaus custom JS
 */

jQuery(document).ready(function(){
 	// iCheck style checkbox and radio button with style square black
	jQuery('input').iCheck({
		checkboxClass: 'icheckbox_square',
		radioClass: 'iradio_square',
		increaseArea: '20%' // optional
	});

	// chosen select styling
	jQuery(".chosen").chosen({
		width: "100%"
	});

	if (jQuery('.nav-horizontal').length > 0) {
		jQuery('.header').prepend('<div id="mobile_nav"><a href="#" class="nav_icon-box"><div class="nav_icon"></div></a></div>');
		jQuery('.block-type-navigation').css('min-height','0px');
		jQuery('#mobile_nav').click(function(e) {
	  		e.preventDefault();
	  		jQuery(this).find('.nav_icon-box').toggleClass('open');
	  		jQuery('.nav-horizontal ul').slideToggle();
	  	});
	}
});


// Stop scrolling for tabs on hash change
jQuery(document).on('touchstart click', 'li.vc_tta-tab a,li.vc_tta-tab,.vc_tta-panel-title', function(){
     jQuery('html, body').stop();
});

jQuery(document).on('touchstart click', '.ult-banner-block.ult-bb-box a.bb-link', function(e){
	if (!jQuery(this).parent().hasClass('vc_tta-tab')) {
	    var target = this.hash;
	    if (target == '#fallstudie1' || target == '#fallstudie2' || target == '#fallstudie3') {
		  	e.preventDefault();
  			e.stopImmediatePropagation();
			jQuery('.vc_tta-tab').find('[href="'+ target + '"]').trigger('click');
	    }
	}
});
