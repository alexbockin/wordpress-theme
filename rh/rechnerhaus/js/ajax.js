jQuery(document).ready(function($) {
    var $mainContent = $("#content").parent(),
        siteUrl = "http://" + top.location.host.toString(),
        url = ''; 
    
    $(document).delegate("a[href^='"+siteUrl+"']:not([href*='/wp-admin/']):not([href*='/wp-login.php']):not([href$='/feed/']):not([href$='/api/'])", "click", function() {
        // if ajax link is enabled
        if (!$(this).parent().hasClass('ajaxcall')) {
            window.location.href = $(this).attr('href');
        } else {
            // change navigation
            $('.menu-item').removeClass('current_page_item');
            $(this).parent().addClass('current_page_item');
            if($('.slicknav_nav').length > 0) {
                $('.slicknav_nav').addClass('slicknav_hidden');
                $('.slicknav_nav').attr('aria-hidden', 'true').hide();
            }

            // change slug
            var splitUrl = wordpressUrl.replace('http://', '').replace(top.location.host.toString(), '');
            var hashTag = this.pathname.replace(splitUrl, '');
            if( hashTag.charAt(0) === '/' ) {
                hashTag = hashTag.slice(1);
            }
            if (hashTag == '') {
                window.location.href = $(this).attr('href');
            }
            location.hash = hashTag;
        }
        return false;
    });

    $("#searchform").submit(function(e) {
        location.hash = '?s=' + $("#s").val();
        e.preventDefault();
    }); 

    // create spinner
	var opts = {
	  lines: 13, // The number of lines to draw
	  length: 0, // The length of each line
	  width: 7, // The line thickness
	  radius: 30, // The radius of the inner circle
	  corners: 1, // Corner roundness (0..1)
	  rotate: 0, // The rotation offset
	  direction: 1, // 1: clockwise, -1: counterclockwise
	  color: '#ffffff', // #rgb or #rrggbb or array of colors
	  speed: 1, // Rounds per second
	  trail: 66, // Afterglow percentage
	  shadow: false, // Whether to render a shadow
	  hwaccel: false, // Whether to use hardware acceleration
	  className: 'spinner', // The CSS class to assign to the spinner
	  zIndex: 2e9, // The z-index (defaults to 2000000000)
	  top: '50%', // Top position relative to parent
	  left: '50%' // Left position relative to parent
	};


    $(window).bind('hashchange', function(){
        url = window.location.hash.substring(1);
        if (!url || url == '!') {
            return;
        }
        loadingContent = '<div id="ajaxspinner"></div>';
        url = url + " #content";
		$mainContent.find('#content').addClass('animated fadeOutLeft');
		// wait for fadeOutLeft animation
		setTimeout(function() {
	       	$mainContent.html(loadingContent);
	       	// load spinner
	       	var target = document.getElementById('ajaxspinner');
	       	var spinner = new Spinner(opts).spin(target);
	       	// load page content with ajax
            $.ajax({
                type: "GET",
                url: url,
                success: function(result) {
                    var scripts = $(result).find("script").add($(result).filter("script")).detach();
                    var htmlFiltered = $(result).find('#content').addClass('animated fadeInRight');
                    $(htmlFiltered).find('.category-news h2').addClass('ajaxcall');
                    // filter the scripts
                    /*var currentScripts = document.getElementsByTagName("script");
                    var resultScripts = [];
                    for (i = 0; i < scripts.length; i++) {
                        var count = 0;
                        for (x = 0; x < currentScripts.length; x++) {
                            if(scripts[i].src === currentScripts[x].src) {
                                count++;
                            }
                        }
                        if (count == 0) {
                            resultScripts.push(scripts[i]);
                        }
                        
                    }
                    console.log(scripts.length);
                    console.log(currentScripts.length);
                    console.log(resultScripts.length);
                    console.log(resultScripts);
                    resultScripts.shift(); */
                    //$('head').append(resultScripts);
                    $mainContent.html(htmlFiltered);
                }
            });
	    }, 1000);
    });
    $(window).trigger('hashchange');
});