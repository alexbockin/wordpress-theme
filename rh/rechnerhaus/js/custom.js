/**
 * Rechnerhaus custom JS
 */

jQuery(document).ready(function(){
 	// iCheck style checkbox and radio button with style square black
	jQuery('input').iCheck({
		checkboxClass: 'icheckbox_square',
		radioClass: 'iradio_square',
		increaseArea: '20%' // optional
	});

	// chosen select styling
	jQuery(".chosen").chosen({
		width: "100%"
	});

	if (jQuery('.nav-horizontal').length > 0) {
		jQuery('.header').prepend('<div id="mobile_nav"><a href="#" class="nav_icon-box"><div class="nav_icon"></div></a></div>');
		jQuery('.block-type-navigation').css('min-height','0px');
		jQuery('#mobile_nav').click(function(e) {
	  		e.preventDefault();
	  		jQuery(this).find('.nav_icon-box').toggleClass('open');
	  		jQuery('.nav-horizontal ul').slideToggle();
	  	});
	}

	// jQuery('.mieten_section').click(function(){
	// 	if (jQuery('.rent_info .wpb_text_column').eq(jQuery('.mieten_section').index(this)).hasClass('active')) {
	// 		jQuery('.rent_info .wpb_text_column').removeClass('active');
	// 		jQuery('.rent_info .wpb_text_column').slideUp();
	// 	} else {
	// 		jQuery('.rent_info .wpb_text_column').removeClass('active');
	// 		jQuery('.rent_info .wpb_text_column').slideUp();
	// 		jQuery('.rent_info .wpb_text_column').eq(jQuery('.mieten_section').index(this)).addClass('active');
	// 		jQuery('.rent_info .wpb_text_column').eq(jQuery('.mieten_section').index(this)).slideToggle();
	// 		// Scroll down
	// 		jQuery("html, body").animate({
	// 	        scrollTop: (jQuery('.rent_info').offset().top - 180)
	// 	    }, 800);	
	// 	}
	// });

	jQuery('.pakete_price .mpc-pricing__property').each(function() {
		currentVal = jQuery(this).text();
		if (currentVal == 'ja') {
			jQuery(this).html('<i class="fa fa-check green_icon"></i>');
		} else 	if (currentVal == 'nein') {
			jQuery(this).html('<i class="fa fa-close red_icon"></i>');
		}
		jQuery(this).show();
	});
	
	// Inline svg image to fill color
	jQuery('.svg_wrap img').each(function(index) {
		var $img = jQuery(this);
		var imgID = $img.attr('id');
		var imgClass = $img.attr('class');
		var imgURL = $img.attr('src');

		if (!imgURL.endsWith('svg')) {
			return;
		}
				
        jQuery.get(imgURL, function(data) {
				// Get the SVG tag, ignore the rest
				var $svg = jQuery(data).find('svg');

				var svgID = $svg.attr('id');

				// Add replaced image's ID to the new SVG if necessary
				if(typeof imgID === 'undefined') {
				    if(typeof svgID === 'undefined') {
						imgID = 'svg-replaced-'+index;
						$svg = $svg.attr('id', imgID);
					} else {
						imgID = svgID;
					}
				} else {
					$svg = $svg.attr('id', imgID);
				}

				// Add replaced image's classes to the new SVG
				if(typeof imgClass !== 'undefined') {
				    $svg = $svg.attr('class', imgClass+' replaced-svg svg-replaced-'+index);
				}

				// Remove any invalid XML tags as per http://validator.w3.org
				$svg = $svg.removeAttr('xmlns:a');

				// Add size attributes
				// $svg = $svg.attr('width', imgWidth);
				// $svg = $svg.attr('height', imgHeight);

				// Replace image with new SVG
				$img.replaceWith($svg);

				jQuery(document).trigger('svg.loaded', [imgID]);
        }, 'xml');
        //jQuery(this).css('opacity', '1');
    });

});


// Stop scrolling for tabs on hash change
jQuery(document).on('touchstart click', 'li.vc_tta-tab a,li.vc_tta-tab,.vc_tta-panel-title', function(){
     jQuery('html, body').stop();
});

jQuery(document).on('touchstart click', '.ult-banner-block.ult-bb-box a.bb-link', function(e){
	if (!jQuery(this).parent().hasClass('vc_tta-tab')) {
	    var target = this.hash;
	    if (target == '#fallstudie1' || target == '#fallstudie2' || target == '#fallstudie3') {
		  	e.preventDefault();
  			e.stopImmediatePropagation();
			jQuery('.vc_tta-tab').find('[href="'+ target + '"]').trigger('click');
	    }
	}
});
