<?php
/*
Intense Template Name: Image on Top
*/

$post = get_post(); 
$intense_recent_post = Intense_Recent_Posts::get_metadata();

$intense_post_type = $intense_recent_post['post_type'];
?>

<article class='recent-post'>

<?php
if ( $intense_recent_post['show_thumbnail'] ) {
    echo intense_run_shortcode( 'intense_post_media', array( 
            'size' => $intense_recent_post['image_size'],
            'image_type' => 'standard',
            'image_shadow' => intense_coalesce( get_field( 'intense_image_shadow' ), '0' ),
            'hover_effect_type' => intense_coalesce( get_field( 'intense_hover_effect_type' ), 'effeckt' ),
            'subtle_effect' => intense_coalesce( get_field( 'intense_subtle_hover_effect' ) ),
            'effeckt' => intense_coalesce( get_field( 'intense_hover_effect' ) ),
            'effeckt_color' => intense_coalesce( get_field( 'intense_effect_color' ) ),
            'effeckt_opacity' => intense_coalesce( get_field( 'intense_effect_opacity' ) ),
            'show_only_first' => $intense_recent_post['layout'] == 'slider',
            'show_missing_image' => $intense_recent_post['show_missing_image'],
            'border_radius' => $intense_recent_post['border_radius'],
            'is_slider' => true,
            'shadow' => 0
        ) );
}

if ( $intense_recent_post['show_title'] ) {
?>
    <h5><a href="<?php echo get_permalink( get_the_ID() ) ?>"><?php echo get_the_title() ?></a></h5>
<?php
}

if ( $intense_recent_post['show_meta'] ) {
    echo intense_run_shortcode( 'intense_metadata', array(
        'type' => 'template',
        'template' => $intense_recent_post['meta_template'],
        'show_comments' => $intense_recent_post['meta_show_comments'],
        'show_author' => $intense_recent_post['meta_show_author'],
        'show_categories' => $intense_recent_post['meta_show_categories'],
        'show_tags' => $intense_recent_post['meta_show_tags'],
        'show_date' => $intense_recent_post['meta_show_date'],
        'date_format' => $intense_recent_post['meta_date_format'],
        'rtl' => is_rtl(),
    ) );
}

if ( $intense_recent_post['show_excerpt'] ) {
?>
    <p class="excerpt">
        <?php echo intense_template_content( $intense_post_type, $intense_recent_post['template_content'], $intense_recent_post['excerpt_length'], 20 ); ?>
    </p>
<?php
}
if ( $intense_recent_post['show_read_more'] ) { ?>
    <span class='read-more pull-right'>
    <?php echo intense_run_shortcode( 'intense_button', array( 'size' => 'mini', 'color' => 'primary', 'link' => esc_url( $intense_recent_post['template_link_url'] ), 'icon' => 'angle-right', 'icon_position' => 'right' ), $intense_recent_post['read_more_text'] ); ?>           
    </span>
    <div class="clearfix"></div>
<?php } ?>
</article>
