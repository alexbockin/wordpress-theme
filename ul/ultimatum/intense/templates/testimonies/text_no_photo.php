<?php
/*
Intense Template Name: Text No Photo
*/

$intense_testimony = Intense_Testimonies::get_metadata(); 

$random_ID = rand();

?>
<div class="testimony">
<?php
echo "<style>";

if ( isset( $intense_testimony['background'] ) || isset( $intense_testimony['font_color'] ) ) {
	if ( isset( $intense_testimony['background'] ) ) {
	  echo "#testimonial_" . esc_attr( $random_ID ) . " { padding: 10px; background-color:" . esc_attr( $intense_testimony['background'] )  . " !important; } #testimonial_" . esc_attr( $random_ID ) . ":after { border-right-color: " . esc_attr( $intense_testimony['background'] )  . " !important; }";
	}

	if ( isset( $intense_testimony['font_color'] ) ) {
	  echo "#testimonial_" . esc_attr( $random_ID ) . " { color:" . esc_attr( $intense_testimony['font_color'] ) . " !important; } #testimonial_" . esc_attr( $random_ID ) . " .quotes { color:" . esc_attr( intense_get_rgb_color( $intense_testimony['font_color'], 50) ) . " !important; }";
	}	
}

echo "</style>";

?>
	<div class="text-content" id="testimonial_<?php echo esc_attr( $random_ID ) ?>">		
		<small><?php echo intense_run_shortcode( 'intense_icon', array( 'source' => Intense()->options['intense_quote_left_icon']['source'], 'type' => Intense()->options['intense_quote_left_icon']['type'] ) ); ?></small>
		<?php echo do_shortcode( $intense_testimony['content'] ); ?>
		<small><?php echo intense_run_shortcode( 'intense_icon', array( 'source' => Intense()->options['intense_quote_right_icon']['source'], 'type' => Intense()->options['intense_quote_right_icon']['type'] ) ); ?></small>
		<div class="clearfix"></div>
		<div class="pull-right" style="font-size: smaller; padding-top: 5px;">
		<?php if ( !empty( $intense_testimony['company'] ) ) { ?>
				<?php echo $intense_testimony['author']; ?><span class="company">,
				<?php if ( !empty( $intense_testimony['link'] ) ) { ?>
					<a href="<?php echo esc_url( $intense_testimony['link'] ); ?>" target='<?php echo esc_attr( $intense_testimony['link_target'] ); ?>'>
				<?php } ?>
				<?php echo $intense_testimony['company']; ?>
				<?php if ( !empty( $intense_testimony['link'] ) ) { ?>
					</a>
				<?php } ?>
				</span>
		<?php } else { ?>
			<?php echo $intense_testimony['author']; ?>
		<?php } ?>
		</div>
		<div class="clearfix"></div>
	</div>	
</div>
