<?php
/*
Intense Template Name: Slider
*/

global $intense_post_media_current_image;

$intense_post_media = Intense_Post_Media::get_metadata();

$include_link = isset( $intense_post_media['include_link'] ) && $intense_post_media['include_link'];

if ( $include_link ) echo "<a href='" . $intense_post_media['permalink'] . "' title='" . $intense_post_media['post_title'] . "' rel='bookmark'>";

if ( !empty( $intense_post_media['is_slider'] ) && $intense_post_media['is_slider'] ) {
	$output = '';

	// if ( !empty( $intense_post_media['shadow'] ) ) {
	// 	$setShadow = " shadow='" . $intense_post_media['shadow'] . "'";
	// } else {
	 	$setShadow = '';
	// }

	$output .= "[intense_slider" . $setShadow . "]";

	$i = 0;

	foreach ( $intense_post_media['image']['images'] as $key => $value) {		
		if ( $i > 0 || !$intense_post_media['hide_first'] ) {
			$intense_post_media_current_image = $value;
			
			$output .= '[intense_slide]';
			
			ob_start();
			require "image.php";
			$output .= ob_get_contents();
			ob_end_clean();

			$output .= '[/intense_slide]';
		}

		$i++;
	}

	$output .= '[/intense_slider]';

	$output = do_shortcode( $output );

	echo $output;
}

if ( $include_link ) echo "</a>";