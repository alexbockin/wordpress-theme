<?php
/*
Intense Template Name: Audio Clip
*/

?>

<div style='padding-top: 10px;'>
<?php
	if ( get_field('intense_book_audio_clip') != '' )  {
		echo intense_run_shortcode( 'intense_audio', array( 'url' => esc_url( get_field('intense_book_audio_clip') ) ) );
	}
?>
</div>