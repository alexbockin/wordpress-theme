<?php
/*
Intense Template Name: One Column Title List
*/

$post = get_post(); 
$intense_custom_post = Intense_Custom_Post::get_metadata();

$intense_post_type = $intense_custom_post['post_type'];
?>
<article class='intense col-lg-12 col-md-12 col-sm-12 col-xs-12 <?php echo esc_attr( $intense_custom_post['post_classes'] ); ?> intense_post nogutter' style='<?php echo esc_attr( $intense_custom_post['plugin_layout_style'] ); ?>' id='post-<?php echo $post->ID; ?>'>
	<!-- Head -->
	<?php echo $intense_custom_post['animation_wrapper_start']; ?>
	<div class='intense row' style='<?php echo esc_attr( $intense_custom_post['cancel_plugin_layout_style'] ); ?>'>
		<div class='intense col-lg-12 col-md-12 col-sm-12 col-xs-12'>
			<div class='post-header'>
				
				<?php echo intense_get_template( '/custom-post/shared/post_title.php', array( 'tag' => 'h2' ) ); ?>

			</div>	
		</div>
	</div>
	<?php echo $intense_custom_post['animation_wrapper_end']; ?>
</article>
