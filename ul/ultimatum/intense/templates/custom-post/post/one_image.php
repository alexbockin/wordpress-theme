<?php
/*
Intense Template Name: One Column (image only)
*/

$post = get_post(); 
$intense_custom_post = Intense_Custom_Post::get_metadata();

?>

<article class='intense col-lg-12 col-md-12 col-sm-12 col-xs-12 <?php echo esc_attr( $intense_custom_post['post_classes'] ); ?> intense_post nogutter' style='<?php echo esc_attr( $intense_custom_post['plugin_layout_style'] ); ?>' id='post-<?php echo $post->ID; ?>'>
	<!-- Head -->
	<?php echo $intense_custom_post['animation_wrapper_start']; ?>
	
	<?php if ( $intense_custom_post['show_images'] ) { ?>
	<a href='<?php echo $intense_custom_post['template_link_url'] ?>' title='<?php echo the_title_attribute( 'echo=0' ); ?>' rel='bookmark'>
	<!-- Image -->
	<div class='intense row' style='<?php echo esc_attr( $intense_custom_post['cancel_plugin_layout_style'] ); ?>'>
		<div class='intense col-lg-12 col-md-12 col-sm-12 col-xs-12' style='position: relative;'>
			
			<?php echo intense_get_template( '/custom-post/shared/post_media.php', array( 'size' => intense_coalesce( $intense_custom_post['image_size'], 'postWide' ), 'include_link' => 0, 'custom_post_image_field' => ( isset( $custom_post_image_field ) ? $custom_post_image_field : null ) ) ); ?>
			
		</div>
	</div>
	</a>
	<?php } ?>
	
	<?php echo $intense_custom_post['animation_wrapper_end']; ?>
</article>
