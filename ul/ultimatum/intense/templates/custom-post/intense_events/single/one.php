<?php
/*
Intense Template Name: One Column
*/

function ult_int_events()
{

	$post = get_post();
	$intense_custom_post = Intense_Custom_Post::get_metadata();

	$padding_top = Intense()->options['intense_layout_row_default_padding']['padding-top'];
	$padding_bottom = Intense()->options['intense_layout_row_default_padding']['padding-bottom'];
	$margin_top = Intense()->options['intense_layout_row_default_margin']['margin-top'];
	$margin_bottom = Intense()->options['intense_layout_row_default_margin']['margin-bottom'];

	$layout_style = '';

	if (!empty($padding_top)) {
		$layout_style .= 'padding-top: ' . $padding_top . '; ';
	}

	if (!empty($padding_bottom)) {
		$layout_style .= 'padding-bottom: ' . $padding_bottom . '; ';
	}

	if (!empty($margin_top)) {
		$layout_style .= 'margin-top: ' . $margin_top . '; ';
	}

	if (!empty($margin_bottom)) {
		$layout_style .= 'margin-bottom: ' . $margin_bottom . '; ';
	}

	$no_layout_style = "padding-top: 0px; padding-bottom: 0px; margin-top: 0px; margin-bottom: 0px;";

	$intense_post_type = Intense()->post_types->get_post_type('intense_events');

	$intense_custom_post = array(
		'plugin_layout_style' => $layout_style,
		'cancel_plugin_layout_style' => $no_layout_style,
		'post_type' => $intense_post_type,
		'taxonomy' => '',
		'template' => '',
		'categories' => '',
		'exclude_categories' => '',
		'exclude_post_ids' => '',
		'authors' => '',
		'order_by' => '',
		'order' => '',
		'posts_per_page' => '',
		'image_size' => 'postExtraWide',
		'image_shadow' => '0',
		'hover_effect_type' => '',
		'hover_effect' => '',
		'subtle_effect' => '',
		'show_all' => '',
		'show_meta' => '0',
		'show_author' => '0',
		'infinite_scroll' => '',
		'show_filter' => '',
		'show_images' => '1',
		'show_missing_image' => '0',
		'show_social_sharing' => '1',
		'show_read_more' => '0',
		'read_more_text' => 'Read More',
		'timeline_mode' => '',
		'timeline_showyear' => '',
		'timeline_readmore' => '',
		'timeline_color' => '',
		'filter_easing' => '',
		'filter_effects' => '',
		'hover_effect_color' => '',
		'hover_effect_opacity' => '',
		'sticky_mode' => '',
		'image_border_radius' => '',
		'timeline_border_radius' => '',
		'animation_type' => '',
		'animation_trigger' => null,
		'animation_scroll_percent' => null,
		'animation_delay' => null,
		'animation_wrapper_start' => null,
		'animation_wrapper_end' => null,
		'is_slider' => ''
	);

	$i = 0;

	do_action('intense_before_main_content');

	while (have_posts()) : the_post();
		$item_classes = '';
		$intense_custom_post['image_shadow'] = '';
		$intense_custom_post['hover_effect_type'] = '';
		$intense_custom_post['hover_subtle_effect_type'] = '';
		$intense_custom_post['hover_effect'] = '';
		$intense_custom_post['hover_effect_color'] = '';
		$intense_custom_post['hover_effect_opacity'] = '';
		$intense_custom_post['index'] = $i;
		$hover = 'effeckt';

		if (has_post_thumbnail()) {
			if (get_field('intense_image_shadow') != '') {
				$intense_custom_post['image_shadow'] = get_field('intense_image_shadow');
			}

			if (get_field('intense_hover_effect_type') != '') {
				$intense_custom_post['hover_effect_type'] = get_field('intense_hover_effect_type');
				$hover = get_field('intense_hover_effect_type');
			}

			if (get_field('intense_hover_effect') != '' && $hover == 'effeckt') {
				$intense_custom_post['hover_effect'] = get_field('intense_hover_effect');

				if (get_field('intense_effect_color') != '') {
					$intense_custom_post['hover_effect_color'] = get_field('intense_effect_color');
				}

				if (get_field('intense_effect_opacity') != '') {
					$intense_custom_post['hover_effect_opacity'] = get_field('intense_effect_opacity');
				}
			} else {
				$intense_custom_post['hover_effect'] = get_field('intense_subtle_hover_effect');
			}
		}

		$intense_custom_post['post_classes'] = $item_classes;
		?>

		<article
			class='intense col-lg-12 col-md-12 col-sm-12 col-xs-12 <?php echo esc_attr($intense_custom_post['post_classes']); ?> intense_post nogutter'
			style='<?php echo esc_attr($intense_custom_post['plugin_layout_style']); ?>'
			id='post-<?php echo $post->ID; ?>' itemscope itemtype='http://data-vocabulary.org/Event'>
			<!-- Head -->
			<?php echo $intense_custom_post['animation_wrapper_start']; ?>


			<?php if ($intense_custom_post['show_images']) { ?>
				<!-- Image -->
				<div class='intense row'
					 style='<?php echo esc_attr($intense_custom_post['cancel_plugin_layout_style']); ?>'>
					<div class='intense col-lg-12 col-md-12 col-sm-12 col-xs-12' style='position: relative;'>
						<?php echo intense_run_shortcode('intense_post_media', array(
							'size' => intense_coalesce($intense_custom_post['image_size'], 'postWide'),
							'image_type' => intense_coalesce(get_field('intense_featured_image_type'), 'standard'),
							'shadow' => $intense_custom_post['image_shadow'],
							'hover_effect_type' => $intense_custom_post['hover_effect_type'],
							'effeckt' => $intense_custom_post['hover_effect'],
							'effeckt_color' => $intense_custom_post['hover_effect_color'],
							'effeckt_opacity' => $intense_custom_post['hover_effect_opacity'],
							'subtle_effect' => $intense_custom_post['subtle_effect'],
							'is_slider' => true,
							'include_link' => 0,
							'itemprop' => 'photo'
						));
						?>
					</div>
				</div>
			<?php } ?>

			<div class='intense row'
				 style='<?php echo esc_attr($intense_custom_post['cancel_plugin_layout_style']); ?>'>
				<div class='intense col-lg-9 col-md-9 col-sm-9 col-xs-12'>
					<div class='post-header'>
						<h1 class='entry-title' style='margin-top:30px;' itemprop='summary'>
							<?php echo the_title_attribute('echo=0'); ?>
						</h1>
						<?php
						if ($intense_post_type->get_subtitle() != '') {
							echo '<h3 style="margin-top:10px; margin-bottom:0;">' . $intense_post_type->get_subtitle() . '</h3>';
						}

						if (get_field('intense_event_start_date') != '') {
							$startdate = get_field('intense_event_start_date');
							$enddate = '';
							$isoEndDate = '';
							$startdate = date("M d, Y", strtotime($startdate));
							$isoStartDate = '<time itemprop="startDate" datetime="' . date("Y-m-d", strtotime(get_field('intense_event_start_date'))) . '">' . $startdate . '</time>';

							if (get_field('intense_event_end_date') != '' && get_field('intense_event_end_date') != get_field('intense_event_start_date')) {
								$enddate = date("M d, Y", strtotime(get_field('intense_event_end_date')));
								$isoEndDate = ' - <time itemprop="endDate" datetime="' . date("Y-m-d", strtotime(get_field('intense_event_end_date'))) . '">' . $enddate . '</time>';
							}

							echo '<h5 style="margin:10px 0;">' . $isoStartDate . ' - ' . $enddate . '</h5>';
						}
						?>
					</div>
				</div>
				<div class='intense col-lg-3 col-md-3 col-sm-3 col-xs-12'>
					<div class='post-header'>
						<?php
						if (get_field('intense_event_entrance_cost') != '') {
							echo '<div style="display:inline-block; width:100%; margin-top:30px; padding-bottom:10px;">';
							echo '<div class="pull-left"><h1 class="entry-title event-price" style="color:green; margin:0;" itemprop="price">' . get_field('intense_event_entrance_cost') . '</h1></div>';


							if (get_field('intense_event_buy_link') != '') {
								echo '<div class="pull-right" style="margin-top:5px; margin-right:5px;">' . intense_run_shortcode('intense_button', array('size' => 'mini', 'color' => 'green', 'link' => get_field('intense_event_buy_link'), 'icon' => 'angle-right', 'icon_position' => 'right'), 'Buy Now') . "</div>";
							}
							echo '</div>';
						}

						echo '<span itemprop="location" itemscope itemtype="http://data-vocabulary.org/Organization">';

						if (get_field('intense_event_venue_name')) {
							echo '<h5 style="padding:0px; margin:0px;"><strong>​<span itemprop="name">' . get_field('intense_event_venue_name') . '</span></strong></h5>';
						}

						if (get_field('intense_event_address') != '') {
							echo '<h5 style="margin-top:5px;"><span itemprop="address">' . get_field('intense_event_address') . '</span><br /><a href="' . get_field('intense_event_website') . '" target="_blank">' . get_field('intense_event_website') . '</a></h5>';
						}

						echo '</span>';
						?>
					</div>
				</div>
			</div>

			<!-- Content -->
			<div class='intense row'
				 style='<?php echo esc_attr($intense_custom_post['cancel_plugin_layout_style']); ?> padding-top: 10px;'>
				<div class='entry-content intense col-lg-12 col-md-12 col-sm-12 col-xs-12' itemprop='description'>
					<?php
					$content = intense_template_content('intense_events', 'full_post', '', 100);
					echo $content;
					?>
				</div>
			</div>

			<!-- Footer -->
			<footer style='padding-top: 5px;'>

			</footer>
			<?php echo $intense_custom_post['animation_wrapper_end']; ?>
		</article>

		<?php

		$i++;
	endwhile;

	do_action('intense_after_main_content');
}
remove_action( 'ultimatum_loop', 'ultimatum_standard_loop' );
add_action('ultimatum_loop', 'ult_int_events');
ultimatum();
