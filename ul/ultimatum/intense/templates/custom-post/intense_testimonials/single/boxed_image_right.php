<?php
/*
Intense Template Name: Boxed (Image Right)
*/

function ult_intense_testimonialbir()
{

	$post = get_post();
	$intense_custom_post = Intense_Custom_Post::get_metadata();

	$padding_top = Intense()->options['intense_layout_row_default_padding']['padding-top'];
	$padding_bottom = Intense()->options['intense_layout_row_default_padding']['padding-bottom'];
	$margin_top = Intense()->options['intense_layout_row_default_margin']['margin-top'];
	$margin_bottom = Intense()->options['intense_layout_row_default_margin']['margin-bottom'];

	$layout_style = '';

	if (!empty($padding_top)) {
		$layout_style .= 'padding-top: ' . $padding_top . '; ';
	}

	if (!empty($padding_bottom)) {
		$layout_style .= 'padding-bottom: ' . $padding_bottom . '; ';
	}

	if (!empty($margin_top)) {
		$layout_style .= 'margin-top: ' . $margin_top . '; ';
	}

	if (!empty($margin_bottom)) {
		$layout_style .= 'margin-bottom: ' . $margin_bottom . '; ';
	}

	$no_layout_style = "padding-top: 0px; padding-bottom: 0px; margin-top: 0px; margin-bottom: 0px;";

	$intense_post_type = Intense()->post_types->get_post_type('intense_testimonials');

	$intense_custom_post = array(
		'plugin_layout_style' => $layout_style,
		'cancel_plugin_layout_style' => $no_layout_style,
		'post_type' => $intense_post_type,
		'taxonomy' => '',
		'template' => '',
		'categories' => '',
		'exclude_categories' => '',
		'exclude_post_ids' => '',
		'authors' => '',
		'order_by' => '',
		'order' => '',
		'posts_per_page' => '',
		'image_size' => 'medium600',
		'image_shadow' => '0',
		'hover_effect_type' => '',
		'hover_effect' => '',
		'subtle_effect' => '',
		'show_all' => '',
		'show_meta' => '0',
		'show_author' => '0',
		'infinite_scroll' => '',
		'show_filter' => '',
		'show_images' => '1',
		'show_missing_image' => '0',
		'show_social_sharing' => '1',
		'show_read_more' => '0',
		'read_more_text' => 'Read More',
		'timeline_mode' => '',
		'timeline_showyear' => '',
		'timeline_readmore' => '',
		'timeline_color' => '',
		'filter_easing' => '',
		'filter_effects' => '',
		'hover_effect_color' => '',
		'hover_effect_opacity' => '',
		'sticky_mode' => '',
		'image_border_radius' => '',
		'timeline_border_radius' => '',
		'animation_type' => '',
		'animation_trigger' => null,
		'animation_scroll_percent' => null,
		'animation_delay' => null,
		'animation_wrapper_start' => null,
		'animation_wrapper_end' => null,
		'is_slider' => ''
	);

	$i = 0;

	do_action('intense_before_main_content');

	while (have_posts()) : the_post();
		$item_classes = '';
		$intense_custom_post['image_shadow'] = '';
		$intense_custom_post['hover_effect_type'] = '';
		$intense_custom_post['hover_subtle_effect_type'] = '';
		$intense_custom_post['hover_effect'] = '';
		$intense_custom_post['hover_effect_color'] = '';
		$intense_custom_post['hover_effect_opacity'] = '';
		$intense_custom_post['index'] = $i;
		$hover = 'effeckt';

		if (has_post_thumbnail()) {
			if (get_field('intense_image_shadow') != '') {
				$intense_custom_post['image_shadow'] = get_field('intense_image_shadow');
			}

			if (get_field('intense_hover_effect_type') != '') {
				$intense_custom_post['hover_effect_type'] = get_field('intense_hover_effect_type');
				$hover = get_field('intense_hover_effect_type');
			}

			if (get_field('intense_hover_effect') != '' && $hover == 'effeckt') {
				$intense_custom_post['hover_effect'] = get_field('intense_hover_effect');

				if (get_field('intense_effect_color') != '') {
					$intense_custom_post['hover_effect_color'] = get_field('intense_effect_color');
				}

				if (get_field('intense_effect_opacity') != '') {
					$intense_custom_post['hover_effect_opacity'] = get_field('intense_effect_opacity');
				}
			} else {
				$intense_custom_post['hover_effect'] = get_field('intense_subtle_hover_effect');
			}
		}

		$intense_custom_post['post_classes'] = $item_classes;
		?>

		<article
			class='intense col-lg-12 col-md-12 col-sm-12 col-xs-12 <?php echo esc_attr($intense_custom_post['post_classes']); ?> intense_post nogutter'
			style='<?php echo esc_attr($intense_custom_post['plugin_layout_style']); ?>'
			id='post-<?php echo $post->ID; ?>'>
			<!-- Head -->
			<?php echo $intense_custom_post['animation_wrapper_start']; ?>

			<div class='intense row'
				 style='<?php echo esc_attr($intense_custom_post['cancel_plugin_layout_style']); ?>'>
				<div class='intense col-lg-8 col-md-8 col-sm-8 col-xs-12'>
					<?php

					$intense_testimony = array(
						'author' => get_field('intense_testimonial_author'),
						'company' => get_field('intense_testimonial_company'),
						'link' => get_field('intense_testimonial_link'),
						'link_target' => get_field('intense_testimonial_link_target'),
						'image' => get_field('intense_testimonial_author_image'),
						'background' => get_field('intense_testimonial_background'),
						'font_color' => get_field('intense_testimonial_font_color'),
						'content' => get_the_content(),
					);

					echo do_shortcode('[intense_testimonies source="manual" template="boxed"]
					[intense_testimony 
					author="' . $intense_testimony['author'] . '" 
					company="' . $intense_testimony['company'] . '" 
					link="' . $intense_testimony['link'] . '" 
					link_target="' . $intense_testimony['link_target'] . '" 
					image="' . $intense_testimony['image'] . '" 
					background="' . $intense_testimony['background'] . '" 
					font_color="' . $intense_testimony['font_color'] . '"]
					' . $intense_testimony['content'] . '
					[/intense_testimony]
					[/intense_testimonies]');
					?>
				</div>
				<div class='intense col-lg-4 col-md-4 col-sm-4 col-xs-12' style='padding:5px;'>
					<?php if ($intense_custom_post['show_images']) {
						echo intense_run_shortcode('intense_post_media', array(
							'size' => intense_coalesce($intense_custom_post['image_size'], 'medium800'),
							'image_type' => intense_coalesce(get_field('intense_featured_image_type'), 'standard'),
							'shadow' => $intense_custom_post['image_shadow'],
							'hover_effect_type' => $intense_custom_post['hover_effect_type'],
							'effeckt' => $intense_custom_post['hover_effect'],
							'effeckt_color' => $intense_custom_post['hover_effect_color'],
							'effeckt_opacity' => $intense_custom_post['hover_effect_opacity'],
							'subtle_effect' => $intense_custom_post['subtle_effect'],
							'is_slider' => true,
							'include_link' => 0
						));
					} ?>
				</div>
			</div>

			<!-- Footer -->
			<footer style='padding-top: 5px;'>
				<div class='intense row'
					 style='<?php echo esc_attr($intense_custom_post['cancel_plugin_layout_style']); ?> padding-top: 5px;'>

				</div>
			</footer>
			<?php echo $intense_custom_post['animation_wrapper_end']; ?>
		</article>

		<?php

		$i++;
	endwhile;

	do_action('intense_after_main_content');
}
remove_action( 'ultimatum_loop', 'ultimatum_standard_loop' );
add_action('ultimatum_loop', 'ult_intense_testimonialbir');
ultimatum();
