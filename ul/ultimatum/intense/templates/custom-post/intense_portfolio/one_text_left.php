<?php
/*
Intense Template Name: One Column (text left)
*/

$post = get_post(); 
$intense_custom_post = Intense_Custom_Post::get_metadata();

$intense_post_type = $intense_custom_post['post_type'];
?>
<article class='intense row <?php echo esc_attr( $intense_custom_post['post_classes'] ); ?> intense_post nogutter' style='<?php echo esc_attr( $intense_custom_post['plugin_layout_style'] ); ?>' id='post-<?php echo $post->ID; ?>'>
	<?php echo $intense_custom_post['animation_wrapper_start']; ?>
	<!-- Image -->
	<div class='intense col-lg-7 col-md-7 col-sm-7 col-xs-7 pull-right' style="margin-left: 10px;">

		<?php echo intense_get_template( '/custom-post/shared/post_media.php', array( 'size' => intense_coalesce( $intense_custom_post['image_size'], 'medium640' ) ) ); ?>
		
	</div>

	<!-- Head -->
	<div class='post-header'>

		<?php echo intense_get_template( '/custom-post/shared/post_title.php', array( 'tag' => 'h2' ) ); ?>
		
	</div>	

	<div class='post-category'>
		<h6><?php 
		$categories = explode(", ", $intense_custom_post['categories']);

		foreach ($categories as $category) {
			echo intense_run_shortcode( 'intense_badge', null, $category ) . ' ';
		}
		?></h6>
	</div>

	<!-- Content -->	
	<div class='entry-content'>
		<?php echo intense_template_content( $intense_post_type, $intense_custom_post['template_content'], $intense_custom_post['template_content_length'], 45 ); ?>
	</div>

	<div class="clearfix"></div>

	<!-- Footer -->
	<footer style='padding-top: 5px;'>
		
		<?php echo intense_get_template( '/custom-post/shared/post_read_more.php' ); ?>

	</footer>
	<?php echo $intense_custom_post['animation_wrapper_end']; ?>
</article>
