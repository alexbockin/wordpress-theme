<?php
/*
Intense Template Name: Post Metadata
*/

$intense_custom_post = Intense_Custom_Post::get_metadata();

if ( $intense_custom_post['show_meta'] ) {	
	echo intense_run_shortcode( 'intense_metadata', array(
		'type' => 'template',
		'template' => $intense_custom_post['meta_template'],
		'show_comments' => $intense_custom_post['meta_show_comments'],
		'show_author' => $intense_custom_post['meta_show_author'],
		'show_categories' => $intense_custom_post['meta_show_categories'],
		'show_tags' => $intense_custom_post['meta_show_tags'],
		'show_date' => $intense_custom_post['meta_show_date'],
		'date_format' => $intense_custom_post['meta_date_format'],
	) );
}

