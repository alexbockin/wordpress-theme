<?php
/*
Intense Template Name: Post Social Icons
*/

$intense_custom_post = Intense_Custom_Post::get_metadata(); 

if ( empty( $position ) ) $position = 'right';

?>

<span class="pull-<?php echo esc_attr( $position ); ?>">
	<?php 
		$setsources = array( 'intense_member_website','intense_member_facebook','intense_member_googleplus','intense_member_twitter','intense_member_instagram','intense_member_youtube','intense_member_dribbble','intense_member_linkedin' );
		$seticons = array( 'globe','facebook','google-plus','twitter','instagram','youtube','dribbble','linkedin' );
		$setlabels = array( 'Website','Facebook','Google+','Twitter','Instagram','Youtube','Dribbble','LinkedIn' );

		$i = 0;

		foreach ( $setsources as $value ) {
			$field_value = get_field( $value );

		    if ( !empty( $field_value ) ) {
				echo intense_run_shortcode( 'intense_social_icon', array( 
					'mode' => 'fontawesome',
					'type' => esc_attr( $seticons[$i] ),
					'label' => esc_attr( $setlabels[$i] ),
					'link' => esc_url( $field_value  ),
					'link_target' => '_blank',
					'size' => '20',
					'color' => 'primary'
				) );
		    }

		    $i++;
		}
    ?>
</span>

<?php if ( $position == 'right' ): ?>
<div class="clearfix"></div>
<?php endif; ?>
