<?php
/*
Intense Template Name: Post Read More
*/

$intense_custom_post = Intense_Custom_Post::get_metadata();

if ( empty( $position ) ) $position = 'right';

if ( $intense_custom_post['show_read_more'] ) { ?>
	<span class="read-more pull-<?php echo esc_attr( $position ); ?>">
		<?php 
		echo intense_run_shortcode( 'intense_button', array( 
			'size' => 'mini', 
			'color' => 'link', 
			'link' => esc_url( $intense_custom_post['template_link_url'] ), 
			'icon' => 'angle-right', 
			'icon_position' => 'right' ), $intense_custom_post['read_more_text'] 
		); 
		?>			
	</span>
	<?php if ( $position == 'right' ): ?>
	<div class="clearfix"></div>
	<?php endif; ?>
<?php
}