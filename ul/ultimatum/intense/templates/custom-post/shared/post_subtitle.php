<?php
/*
Intense Template Name: Post Title
*/
$intense_custom_post = Intense_Custom_Post::get_metadata();

$intense_post_type = $intense_custom_post['post_type'];

if ( is_object( $intense_post_type ) && $intense_post_type->get_subtitle() != '' ) {
	echo '<' . esc_attr( $tag ) . ' ' . ( isset( $margin ) ? 'style="margin: ' . esc_attr( $margin ) . ';"' : '' ) . '>' . $intense_post_type->get_subtitle() . '</' . esc_attr( $tag ) . '>';
}