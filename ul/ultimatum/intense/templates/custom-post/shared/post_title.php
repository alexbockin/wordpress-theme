<?php
/*
Intense Template Name: Post Title
*/
$intense_custom_post = Intense_Custom_Post::get_metadata();

$show_link = !empty( $intense_custom_post['template_link_url'] );
$before = $show_link ? '<a href="' . esc_url( $intense_custom_post['template_link_url'] ) . '" title="' . the_title_attribute( 'echo=0' ) . '" rel="bookmark">' : '';
$after = $show_link ? '</a>' : '';
?>

<<?php echo esc_attr( $tag ); ?> class="entry-title" <?php echo ( isset( $margin ) ? 'style="margin: ' . esc_attr( $margin ) . ';"' : '' ); ?>><?php the_title( $before, $after, true ); ?><?php echo $intense_custom_post['edit_link']; ?></<?php echo esc_attr( $tag ); ?>>