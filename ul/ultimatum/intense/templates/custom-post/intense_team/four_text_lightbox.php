<?php
/*
Intense Template Name: Four Columns (text w/ Lightbox)
*/

$post = get_post(); 
$intense_custom_post = Intense_Custom_Post::get_metadata();

$inline = ( is_sticky() && 'inline' == $intense_custom_post['sticky_mode'] );

if ( $inline ) {
	$span = "12";
	$size = "postWide";
	$header_tag = "h3";
} else {
	$span = "3";
	$size = ( isset( $intense_custom_post['image_size'] ) ? $intense_custom_post['image_size'] : 'medium640' );
	$header_tag = "h4";
}

$padding_bottom = Intense()->options['intense_layout_row_default_padding']['padding-top'];
$intense_post_type = $intense_custom_post['post_type'];
$random_id = 'img_' . rand();
$content = '';
$lightbox_content = '';

if ( $intense_custom_post['show_images'] ) { 
	$image = get_field( 'intense_member_photo' );

	if( !empty( $image ) ) {
		$content .= "<div style='padding:10px 0;'>" . intense_run_shortcode( 'intense_image', array( 'size' => ( $intense_custom_post['image_size'] === '' ? 'medium500' : $intense_custom_post['image_size'] ), 'image' => $image["id"], 'border_radius' => ( $intense_custom_post['image_border_radius'] === '' ? '0px' : $intense_custom_post['image_border_radius'] ) ) ) . "</div>";
	}
}

$content .= "<center><" .  $header_tag . " class='entry-title' style='margin:10px 0;'>" . the_title_attribute( 'echo=0' ) . "</" . $header_tag . "><div class='entry-content'>";		

if ( get_field( 'intense_member_title' ) != '' ) {
	$content .= '<h5 style="margin:5px 0;">' . get_field( 'intense_member_title' ) . '</h5>';
}

$content .= "</div></center>";

$lightbox_content .= '[intense_row padding_top="0" padding_bottom="5"][intense_column size="3" medium_size="3" small_size="12" extra_small_size="12"]';
$lightbox_content .= intense_run_shortcode( 'intense_image', array( 'size' => ( $intense_custom_post['image_size'] === '' ? 'medium500' : $intense_custom_post['image_size'] ), 'image' => $image["id"], 'border_radius' => ( $intense_custom_post['image_border_radius'] === '' ? '0px' : $intense_custom_post['image_border_radius'] ) ) );
$lightbox_content .= '[/intense_column]';
$lightbox_content .= '[intense_column size="9" medium_size="9" small_size="12" extra_small_size="12"]';

$lightbox_content .= '<div class="pull-right" style="position:absolute; right: 20px;">' . intense_get_template( '/custom-post/shared/post_social_icons.php' ) . '</div>';

$lightbox_content .= "<" .  $header_tag . " class='entry-title' style='margin:10px 0;'>" . the_title_attribute( 'echo=0' ) . "</" . $header_tag . ">";

if ( !empty( get_field( 'intense_member_title' ) ) ) {
	$lightbox_content .= '<h5 style="margin:5px 0;">' . get_field( 'intense_member_title' ) . '</h5>';
}

if ( !empty( get_field( 'intense_member_personal_phone' ) ) ) {
	$lightbox_content .= intense_run_shortcode( 'intense_icon', array( 'type' => 'mobile' ) ) . " " . get_field( 'intense_member_personal_phone' ) . "<br />";
}

if ( !empty( get_field( 'intense_member_office_phone' ) ) ) {
	$lightbox_content .= intense_run_shortcode( 'intense_icon', array( 'type' => 'phone' ) ) . " " . get_field( 'intense_member_office_phone' ) . "<br />";
}

if ( !empty( get_field( 'intense_member_email' ) ) ) {
	$lightbox_content .= intense_run_shortcode( 'intense_icon', array( 'type' => 'envelope' ) ) . " " . get_field( 'intense_member_email' ) . "<br />";
}

$lightbox_content .= '[/intense_column]';
$lightbox_content .= '[/intense_row]';
$lightbox_content .= intense_template_content( $intense_post_type, $intense_custom_post['template_content'], $intense_custom_post['template_content_length'], 120 );
?>
<div class='intense col-lg-<?php echo esc_attr( $span ); ?> col-md-<?php echo esc_attr( $span ); ?> col-sm-6 col-xs-12  <?php echo esc_attr( $intense_custom_post['post_classes'] ); ?> intense_post nogutter' style='margin-left: 0px; float: none; padding: 0 10px; padding-bottom: <?php echo esc_attr( $padding_bottom ); ?>; display:inline-block; vertical-align: top;'>
	<article id='post-<?php echo $post->ID; ?>' class='<?php echo ( esc_attr( $inline ) ?  'featured ' : '' ); ?>'>
	<?php echo $intense_custom_post['animation_wrapper_start']; ?>
		<?php 
			echo intense_run_shortcode( 'intense_lightbox', array( 'type' => 'magnificpopup' , 'content_type' => 'html', 'html_element' => $random_id ), $content );
		?>
		<div id='<?php echo $random_id; ?>' style='width:800px; padding:30px 15px; background-color:#ffffff; margin:0 auto;'>
		<?php
			echo do_shortcode( $lightbox_content );
		?>
		</div>
	<?php echo $intense_custom_post['animation_wrapper_end']; ?>
	</article>
</div>