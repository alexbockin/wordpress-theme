<?php
/*
Intense Template Name: Supersized Controls
*/

$control_color = Intense()->options['intense_supersized_color'];
$control_font_color = intense_get_contrast_color( $control_color );
$navigation_color = intense_get_plugin_color( "primary" );

// darken the font a little so it isn't as blaring
// against dark colors
if ( $control_font_color == 'white' ) {
    $control_font_color = "#ccc";
}

?>
<style>
    #controlskew, #titleskew, #captionskew {
        background: <?php echo $control_color; ?> !important;
    }

    #captionskew, #slidecaption, #slidecounter, #slideDescription {
        color: <?php echo $control_font_color; ?> !important;
    }

    #colorskew {
            background-color: <?php echo $navigation_color; ?> !important;
    }
    #pauseplay, #prevslide, #nextslide {
        color: <?php echo $navigation_color; ?> !important;
    }
    #tray-button {
        color: <?php echo $navigation_color; ?> !important;
    }
</style>
<!-- Start of Supersized Navigation Controls -->
<!--Supersized Thumbnail Navigation-->
<div id="prevthumb"></div> <div id="nextthumb"></div>

<div id="controlskew"></div>
<div id="colorskew"></div>
<div id="titleskew"></div>
<div id="captionskew"></div>
<div id="slideDescription"></div>

<a href="javascript: void(0)" id="prevslide" title="Slide Left">
	<?php echo intense_run_shortcode( 'intense_icon', array( 
			'source' => Intense()->options['intense_previous_icon']['source'], 
			'type' => Intense()->options['intense_previous_icon']['type'] 
		)); ?>
</a>
<a href="javascript: void(0)" id="nextslide" title="Slide Right">
	<?php echo intense_run_shortcode( 'intense_icon', array( 
			'source' => Intense()->options['intense_next_icon']['source'], 
			'type' => Intense()->options['intense_next_icon']['type'] 
		)); ?>
</a>
<a id="pauseplay">
	<?php echo intense_run_shortcode( 'intense_icon', array( 
			'source' => Intense()->options['intense_pause_icon']['source'], 
			'type' => Intense()->options['intense_pause_icon']['type'] 
		)); ?>
</a>
<a id="tray-button" title="Show/Hide thumbnails">
	<?php echo intense_run_shortcode( 'intense_icon', array( 
			'source' => Intense()->options['intense_expand_icon']['source'], 
			'type' => Intense()->options['intense_expand_icon']['type'] 
		)); ?>
</a>
<a id="fullscreen-button" title="Toggle Full Screen">
	<?php echo intense_run_shortcode( 'intense_icon', array( 
			'source' => Intense()->options['intense_fullscreen_icon']['source'], 
			'type' => Intense()->options['intense_fullscreen_icon']['type'] 
		)); ?>
</a>

<div id="thumb-tray" class="load-item">
    <div id="thumb-back"></div>
    <div id="thumb-forward"></div>
</div>

<!--Time Bar-->
<div id="progress-back" class="load-item">
    <div id="progress-bar"></div>
</div>

<!--Control Bar-->
<div id="controls-wrapper">
    <div id="controls">

        <!--Slide counter-->
        <div id="slidecounter">
            <span class="slidenumber"></span> / <span class="totalslides"></span>
        </div>

        <!--Slide captions displayed here-->
        <div id="slidecaption"></div>

        <!--Navigation-->
        <!-- <ul id="slide-list"></ul> -->
    </div>
</div>
<!-- End of Supersized -->';