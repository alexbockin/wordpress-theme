<?php
/*
Intense Template Name: Product Sale Dot
*/

$product = wc_get_product();
$post = get_post(); 
$intense_custom_post = Intense_Custom_Post::get_metadata();

$intense_post_type = $intense_custom_post['post_type'];

if ( ! $product || ! $product->is_visible() ) {
	return;
}

if ( $product->is_on_sale() ) {
	echo apply_filters( 'woocommerce_sale_flash', '<span class="onsale dot">' . __( 'Sale!', 'woocommerce' ) . '</span>', $post, $product );
}
