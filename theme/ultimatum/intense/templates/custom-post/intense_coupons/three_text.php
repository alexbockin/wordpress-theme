<?php
/*
Intense Template Name: Three Columns (text)
*/

$post = get_post(); 
$intense_custom_post = Intense_Custom_Post::get_metadata();

$inline = ( is_sticky() && 'inline' == $intense_custom_post['sticky_mode'] );

if ( $inline ) {
	$span = "12";
	$size = "postWide";
	$header_tag = "h3";
} else {
	$span = "4";
	$size = ( isset( $intense_custom_post['image_size'] ) ? $intense_custom_post['image_size'] : 'medium640' );
	$header_tag = "h4";
}

$padding_bottom = Intense()->options['intense_layout_row_default_padding']['padding-top'];
$intense_post_type = $intense_custom_post['post_type'];
?>
<div class='intense col-lg-<?php echo esc_attr( $span ); ?> col-md-<?php echo esc_attr( $span ); ?> col-sm-6 col-xs-12  <?php echo esc_attr( $intense_custom_post['post_classes'] ); ?> intense_post nogutter' style='margin-left: 0px; float: none; padding: 0 10px; padding-bottom: <?php echo esc_attr( $padding_bottom ); ?>; display:inline-block; vertical-align: top;'>
	<article id='post-<?php echo $post->ID; ?>' class='<?php echo ( esc_attr( $inline ) ?  'featured ' : '' ); ?>'>
	<?php echo $intense_custom_post['animation_wrapper_start']; ?>
		<div style="border: 3px dashed #cccccc; padding:10px;">
		
			<?php echo intense_get_template( '/custom-post/shared/post_title.php', array( 'tag' => $header_tag, 'margin' => '5px 0' ) ); ?>
			
			<div class='entry-content'>
				<?php echo do_shortcode( get_the_content() ); ?>
			</div>
			<div style='color:#333333;font-size:10px; padding-top:20px;'>
			<?php
				if ( get_field( 'intense_coupon_start_date' ) != '' && get_field( 'intense_coupon_end_date' ) != '' ) {
					echo __( 'Valid from', 'intense' ) . ' ' . date("M d, Y", strtotime( get_field( 'intense_coupon_start_date' ) ) ) . ' ' . __( 'to', 'intense' ) . ' ' . date("M d, Y", strtotime( get_field( 'intense_coupon_end_date' ) ) );
				} elseif ( get_field( 'intense_coupon_start_date' ) != '' && get_field( 'intense_coupon_end_date' ) === '' ) {
					echo __( 'Valid after', 'intense' ) . ' ' . date("M d, Y", strtotime( get_field( 'intense_coupon_start_date' ) ) );
				} elseif ( get_field( 'intense_coupon_start_date' ) === '' && get_field( 'intense_coupon_end_date' ) != '' ) {
					echo __( 'Valid until', 'intense' ) . ' ' . date("M d, Y", strtotime( get_field( 'intense_coupon_end_date' ) ) );
				}
			?>
			</div>
		</div>
	<?php echo $intense_custom_post['animation_wrapper_end']; ?>
	</article>
</div>