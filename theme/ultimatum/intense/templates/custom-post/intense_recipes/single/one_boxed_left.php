<?php
/*
Intense Template Name: One Column Boxed (text left)
*/

function ult_intense_recipe_bl(){
	$post = get_post();
	$intense_custom_post = Intense_Custom_Post::get_metadata();

	$padding_top = Intense()->options['intense_layout_row_default_padding']['padding-top'];
	$padding_bottom = Intense()->options['intense_layout_row_default_padding']['padding-bottom'];
	$margin_top = Intense()->options['intense_layout_row_default_margin']['margin-top'];
	$margin_bottom = Intense()->options['intense_layout_row_default_margin']['margin-bottom'];

	$layout_style = '';

	if ( !empty( $padding_top ) ) {
		$layout_style .= 'padding-top: ' . $padding_top . '; ';
	}

	if ( !empty( $padding_bottom ) ) {
		$layout_style .= 'padding-bottom: ' . $padding_bottom . '; ';
	}

	if ( !empty( $margin_top ) ) {
		$layout_style .= 'margin-top: ' . $margin_top . '; ';
	}

	if ( !empty( $margin_bottom ) ) {
		$layout_style .= 'margin-bottom: ' . $margin_bottom . '; ';
	}

	$no_layout_style = "padding-top: 0px; padding-bottom: 0px; margin-top: 0px; margin-bottom: 0px;";

	$intense_post_type = Intense()->post_types->get_post_type( 'intense_recipes' );

	$intense_custom_post = array(
		'plugin_layout_style' => $layout_style,
		'cancel_plugin_layout_style' => $no_layout_style,
		'post_type' => $intense_post_type,
		'taxonomy' => '',
		'template' => '',
		'categories' => '',
		'exclude_categories' => '',
		'exclude_post_ids' => '',
		'authors' => '',
		'order_by' => '',
		'order' => '',
		'posts_per_page' => '',
		'image_size' => 'postFull',
		'image_shadow' => '0',
		'hover_effect_type' => '',
		'hover_effect' => '',
		'subtle_effect' => '',
		'show_all' => '',
		'infinite_scroll' => '',
		'show_filter' => '',
		'show_images' => '1',
		'show_missing_image' => '0',
		'show_social_sharing' => '1',
		'show_read_more' => '0',
		'read_more_text' => 'Read More',
		'timeline_mode' => '',
		'timeline_showyear' => '',
		'timeline_readmore' => '',
		'timeline_color' => '',
		'filter_easing' => '',
		'filter_effects' => '',
		'hover_effect_color' => '',
		'hover_effect_opacity' => '',
		'sticky_mode' => '',
		'image_border_radius' => '',
		'timeline_border_radius' => '',
		'animation_type' => '',
		'animation_trigger' => null ,
		'animation_scroll_percent' => null,
		'animation_delay' => null,
		'animation_wrapper_start' => null,
		'animation_wrapper_end' => null,
		'is_slider' => ''
	);

	$i = 0;

	do_action( 'intense_before_main_content' );

	while ( have_posts() ) : the_post();
	$item_classes = '';
	$intense_custom_post['image_shadow'] = '';
	$intense_custom_post['hover_effect_type'] = '';
	$intense_custom_post['hover_subtle_effect_type'] = '';
	$intense_custom_post['hover_effect'] = '';
	$intense_custom_post['hover_effect_color'] = '';
	$intense_custom_post['hover_effect_opacity'] = '';
	$intense_custom_post['index'] = $i;
	$hover = 'effeckt';

	if ( has_post_thumbnail() ) {
		if ( get_field( 'intense_image_shadow' ) != '' ) {
			$intense_custom_post['image_shadow'] = get_field( 'intense_image_shadow' );
		}

		if ( get_field( 'intense_hover_effect_type' ) != '' ) {
			$intense_custom_post['hover_effect_type'] = get_field( 'intense_hover_effect_type' );
			$hover = get_field( 'intense_hover_effect_type' );
		}

		if ( get_field( 'intense_hover_effect' ) != '' && $hover == 'effeckt' ) {
			$intense_custom_post['hover_effect'] = get_field( 'intense_hover_effect' );

			if ( get_field( 'intense_effect_color' ) != '' ) {
				$intense_custom_post['hover_effect_color'] = get_field( 'intense_effect_color' );
			}

			if ( get_field( 'intense_effect_opacity' ) != '' ) {
				$intense_custom_post['hover_effect_opacity'] = get_field( 'intense_effect_opacity' );
			}
		} else {
			$intense_custom_post['hover_effect'] = get_field( 'intense_subtle_hover_effect' );
		}
	}

	if ( current_user_can( 'manage_options' ) ) {
		$intense_custom_post['edit_link'] = "&nbsp;" . intense_run_shortcode( 'intense_button', array( 'size' => 'mini', 'color' => 'inverse', 'class' => 'post-edit-link' , 'link' => get_edit_post_link( $post->ID ), 'title' => __( "Edit Post", "intense" ) ), __( "Edit", "intense" ) );
	} else {
		$intense_custom_post['edit_link'] = '';
	}

	$intense_custom_post['post_classes'] = $item_classes;
?>

<article class='intense col-lg-12 col-md-12 col-sm-12 col-xs-12 <?php echo esc_attr( $intense_custom_post['post_classes'] ); ?> intense_post nogutter' style='<?php echo esc_attr( $intense_custom_post['plugin_layout_style'] ); ?> color:#736861;' id='post-<?php echo $post->ID; ?>' itemscope itemtype='http://data-vocabulary.org/Recipe'>
	<!-- Head -->
	<?php echo $intense_custom_post['animation_wrapper_start']; ?>

	<div><div style='margin-bottom: 0;'>
	<div class='intense row' style='<?php echo esc_attr( $intense_custom_post['cancel_plugin_layout_style'] ); ?> background-color: #e8e8e8;'>
		<div class='intense col-lg-6 col-md-6 col-sm-12 col-xs-12 nogutter' style="text-align: center;">
			<?php
			$prep = get_field('intense_recipe_prep_time' );
			$cook = get_field('intense_recipe_cook_time' );
			$total_time = $prep + $cook;
			$prep = intense_convert_minutes_to_ISO_format( $prep, 'prepTime' );
			$cook = intense_convert_minutes_to_ISO_format( $cook, 'cookTime' );
			$total_time = intense_convert_minutes_to_ISO_format( $total_time, 'totalTime' );

			?>
			<div class='intense row recipe-header' style='margin: 0; background-color: #DB532B; color: #e8e8e8; box-shadow: 0 5px 2px rgba(0,0,0,0.1);'>
				<div class='intense col-lg-4 col-md-4 col-sm-12 col-xs-12' style="padding: 0;">
					<div class='entry-content' style='margin: 10px 0; border-right: 1px solid #e8e8e8;'>
						<label><strong><?php echo __('Prep', 'intense' ) ?></strong><h4 style='color: #e8e8e8;'><?php echo $prep; ?></h4></label>
					</div>
				</div>
				<div class='intense col-lg-4 col-md-4 col-sm-12 col-xs-12' style="padding: 0;">
					<div class='entry-content' style='margin: 10px 0; border-right: 1px solid #e8e8e8;'>
						<label><strong><?php echo __('Cook', 'intense' ) ?></strong><h4 style='color: #e8e8e8;'><?php echo $cook; ?></h4></label>
					</div>
				</div>
				<div class='intense col-lg-4 col-md-4 col-sm-12 col-xs-12' style="padding: 0;">
					<div class='entry-content' style='margin: 10px 0;'>
						<label><strong><?php echo __('Ready', 'intense' ) ?></strong><h4 style='color: #e8e8e8;'><?php echo $total_time; ?></h4></label>
					</div>
				</div>
			</div>
			<div class='post-header' style='padding:30px;'>
				<h1 class='entry-title' style='color:#736861; text-shadow: 0 1px rgba(255,255,255,0.5);' itemprop='name'><?php echo the_title_attribute( 'echo=0' ); ?></h1>
			</div>
		</div>
		<div class='intense col-lg-6 col-md-6 col-sm-12 col-xs-12 nogutter'>
			<?php 
				if ( $intense_custom_post['show_images'] ) {
					echo intense_run_shortcode( 'intense_post_media', array( 
							'size' => intense_coalesce( $intense_custom_post['image_size'], 'medium800' ),
							'image_type' => intense_coalesce( get_field( 'intense_featured_image_type' ), 'standard' ),
							'shadow' => $intense_custom_post['image_shadow'],
							'hover_effect_type' => $intense_custom_post['hover_effect_type'],
							'effeckt' => $intense_custom_post['hover_effect'],
							'effeckt_color' => $intense_custom_post['hover_effect_color'],
							'effeckt_opacity' => $intense_custom_post['hover_effect_opacity'],
							'subtle_effect' => $intense_custom_post['subtle_effect'],
							'is_slider' => true,
							'include_link' => 0,
							'itemprop' => 'photo'
						) );
				}
			?>	
		</div>
	</div>
	</div><img src='<?php echo INTENSE_PLUGIN_URL ?>/assets/img/shadow10.png' class='intense shadow' style='vertical-align: top;' alt='' /></div>
	<?php
	$cuisine = get_the_term_list( $post->ID, 'intense_recipes_cuisines', '', ', ', '' );
	$course = get_the_term_list( $post->ID, 'intense_recipes_courses', '', ', ', '' );
	$skill_level = get_the_term_list( $post->ID, 'intense_recipes_skill_levels', '', ', ', '' );
	?>
	<div class='intense row' style='<?php echo esc_attr( $intense_custom_post['cancel_plugin_layout_style'] ); ?> padding-top: 15px;'>
		<div class='entry-content intense col-lg-4 col-md-4 col-sm-12 col-xs-12'>
			<label><strong><?php echo __('Cuisine', 'intense' ) ?>:</strong> <?php echo $cuisine; ?></label>
		</div>
		<div class='entry-content intense col-lg-4 col-md-4 col-sm-12 col-xs-12'>
			<label><strong><?php echo __('Course', 'intense' ) ?>: </strong><?php echo $course; ?></label>
		</div>
		<div class='entry-content intense col-lg-4 col-md-4 col-sm-12 col-xs-12'>
			<label><strong><?php echo __('Skill Level', 'intense' ) ?>: </strong><?php echo $skill_level; ?></label>
		</div>
	</div>
	<hr style="margin: 10px 0;">
	<div class='intense row' style='<?php echo esc_attr( $intense_custom_post['cancel_plugin_layout_style'] ); ?> padding-top: 10px;'>
		<div class='entry-content intense col-lg-12 col-md-12 col-sm-12 col-xs-12' itemprop='summary'>
			<?php 
				$content = intense_template_content( 'intense_recipes', 'full_post', '', 100 ); 
				echo $content;
			?>
		</div>
	</div>
	<div class='intense row' style='<?php echo esc_attr( $intense_custom_post['cancel_plugin_layout_style'] ); ?> padding-top: 10px;'>
		<div class='entry-content intense col-lg-5 col-md-5 col-sm-12 col-xs-12'>
		<?php			

			$rows = get_field('intense_recipe_ingredients');

			if( $rows )
			{ ?>
				<h2 style='color:#736861;'><?php echo  __('Ingredients', 'intense' ) ?>:</h2>
				<label style="font-style: italic;"><?php echo __('Recipe yields', 'intense' ) ?> <span itemprop='yield'><?php the_field('intense_recipe_yield'); ?></span></label>
				<span itemprop="ingredient" itemscope itemtype="http://data-vocabulary.org/RecipeIngredient">
				<ul>
			 	
			 	<?php foreach( $rows as $row ) { 
					$ingredient = $row['intense_recipe_ingredient'];
			 	?>
					<li><span itemprop="amount"><?php echo $row['intense_recipe_amount'] . ' ' . $row['intense_recipe_measurement'] . '</span> <span itemprop="name">' . $ingredient->name . '</span> ' . $row['intense_recipe_note'] ?></li>
				<?php } ?>
			 
				</ul>
				</span>
			<?php }
		?>
		</div>
		<div class='entry-content intense col-lg-7 col-md-7 col-sm-12 col-xs-12'>
		<?php			
			$rows = get_field('intense_recipe_instructions');

			if( $rows )
			{ ?>
				<h2 style='color:#736861;'><?php echo  __('Instructions', 'intense' ) ?>:</h2>
				<div itemprop="instructions">
				<ol>
			 	
			 	<?php foreach( $rows as $row ) { 
					$instructions = $row['intense_recipe_description'];
			 	?>
					<li><?php echo $instructions ?></li>
				<?php } ?>
			 
				</ol>
				</div>
			<?php } ?>
		</div>
	</div>

	<!-- Footer -->
	<footer style='padding-top: 5px;'>

	</footer>
	<?php echo $intense_custom_post['animation_wrapper_end']; ?>
</article>

<?php
	$i++;
	endwhile;
do_action('intense_after_main_content');
}
remove_action( 'ultimatum_loop', 'ultimatum_standard_loop' );
add_action('ultimatum_loop', 'ult_intense_recipe_bl');
ultimatum();
