<?php
/*
Intense Template Name: One Column
*/
function ult_intense_recipe(){
$post = get_post();
$intense_custom_post = Intense_Custom_Post::get_metadata();

$padding_top = Intense()->options['intense_layout_row_default_padding']['padding-top'];
$padding_bottom = Intense()->options['intense_layout_row_default_padding']['padding-bottom'];
$margin_top = Intense()->options['intense_layout_row_default_margin']['margin-top'];
$margin_bottom = Intense()->options['intense_layout_row_default_margin']['margin-bottom'];

$layout_style = '';

if ( !empty( $padding_top ) ) {
	$layout_style .= 'padding-top: ' . $padding_top . '; ';
}

if ( !empty( $padding_bottom ) ) {
	$layout_style .= 'padding-bottom: ' . $padding_bottom . '; ';
}

if ( !empty( $margin_top ) ) {
	$layout_style .= 'margin-top: ' . $margin_top . '; ';
}

if ( !empty( $margin_bottom ) ) {
	$layout_style .= 'margin-bottom: ' . $margin_bottom . '; ';
}

$no_layout_style = "padding-top: 0px; padding-bottom: 0px; margin-top: 0px; margin-bottom: 0px;";

$intense_post_type = Intense()->post_types->get_post_type( 'intense_recipes' );

$intense_custom_post = array(
	'plugin_layout_style' => $layout_style,
	'cancel_plugin_layout_style' => $no_layout_style,
	'post_type' => $intense_post_type,
	'taxonomy' => '',
	'template' => '',
	'categories' => '',
	'exclude_categories' => '',
	'exclude_post_ids' => '',
	'authors' => '',
	'order_by' => '',
	'order' => '',
	'posts_per_page' => '',
	'image_size' => 'postFull',
	'image_shadow' => '0',
	'hover_effect_type' => '',
	'hover_effect' => '',
	'subtle_effect' => '',
	'show_all' => '',
	'infinite_scroll' => '',
	'show_filter' => '',
	'show_images' => '1',
	'show_missing_image' => '0',
	'show_social_sharing' => '1',
	'show_read_more' => '0',
	'read_more_text' => 'Read More',
	'timeline_mode' => '',
	'timeline_showyear' => '',
	'timeline_readmore' => '',
	'timeline_color' => '',
	'filter_easing' => '',
	'filter_effects' => '',
	'hover_effect_color' => '',
	'hover_effect_opacity' => '',
	'sticky_mode' => '',
	'image_border_radius' => '',
	'timeline_border_radius' => '',
	'animation_type' => '',
	'animation_trigger' => null ,
	'animation_scroll_percent' => null,
	'animation_delay' => null,
	'animation_wrapper_start' => null,
	'animation_wrapper_end' => null,
	'is_slider' => ''
);

$i = 0;

do_action( 'intense_before_main_content' );

while ( have_posts() ) : the_post();
	$item_classes = '';
	$intense_custom_post['image_shadow'] = '';
	$intense_custom_post['hover_effect_type'] = '';
	$intense_custom_post['hover_subtle_effect_type'] = '';
	$intense_custom_post['hover_effect'] = '';
	$intense_custom_post['hover_effect_color'] = '';
	$intense_custom_post['hover_effect_opacity'] = '';
	$intense_custom_post['index'] = $i;
	$hover = 'effeckt';

	if ( has_post_thumbnail() ) {
		if ( get_field( 'intense_image_shadow' ) != '' ) {
			$intense_custom_post['image_shadow'] = get_field( 'intense_image_shadow' );
		}

		if ( get_field( 'intense_hover_effect_type' ) != '' ) {
			$intense_custom_post['hover_effect_type'] = get_field( 'intense_hover_effect_type' );
			$hover = get_field( 'intense_hover_effect_type' );
		}

		if ( get_field( 'intense_hover_effect' ) != '' && $hover == 'effeckt' ) {
			$intense_custom_post['hover_effect'] = get_field( 'intense_hover_effect' );

			if ( get_field( 'intense_effect_color' ) != '' ) {
				$intense_custom_post['hover_effect_color'] = get_field( 'intense_effect_color' );
			}

			if ( get_field( 'intense_effect_opacity' ) != '' ) {
				$intense_custom_post['hover_effect_opacity'] = get_field( 'intense_effect_opacity' );
			}
		} else {
			$intense_custom_post['hover_effect'] = get_field( 'intense_subtle_hover_effect' );
		}
	}

	$intense_custom_post['post_classes'] = $item_classes;
?>

<article class='intense col-lg-12 col-md-12 col-sm-12 col-xs-12 <?php echo esc_attr( $intense_custom_post['post_classes'] ); ?> intense_post nogutter' style='<?php echo esc_attr( $intense_custom_post['plugin_layout_style'] ); ?>' id='post-<?php echo $post->ID; ?>' itemscope itemtype='http://data-vocabulary.org/Recipe'>
	<!-- Head -->
	<?php echo $intense_custom_post['animation_wrapper_start']; ?>

		
	<?php if ( $intense_custom_post['show_images'] ) { ?>
	<!-- Image -->
	<div class='intense row' style='<?php echo esc_attr( $intense_custom_post['cancel_plugin_layout_style'] ); ?>'>
		<div class='intense col-lg-12 col-md-12 col-sm-12 col-xs-12' style='position: relative;'>
			<?php echo intense_run_shortcode( 'intense_post_media', array( 
						'size' => intense_coalesce( $intense_custom_post['image_size'], 'postWide' ),
						'image_type' => intense_coalesce( get_field( 'intense_featured_image_type' ), 'standard' ),
						'shadow' => $intense_custom_post['image_shadow'],
						'hover_effect_type' => $intense_custom_post['hover_effect_type'],
						'effeckt' => $intense_custom_post['hover_effect'],
						'effeckt_color' => $intense_custom_post['hover_effect_color'],
						'effeckt_opacity' => $intense_custom_post['hover_effect_opacity'],
						'subtle_effect' => $intense_custom_post['subtle_effect'],
						'is_slider' => true,
						'include_link' => 0,
						'itemprop' => 'photo'
					) );
			?>
		</div>
	</div>
	<?php } ?>

	<div class='intense row' style='<?php echo esc_attr( $intense_custom_post['cancel_plugin_layout_style'] ); ?>'>
		<div class='intense col-lg-12 col-md-12 col-sm-12 col-xs-12'>
			<div class='post-header'>
				<a href='<?php echo get_permalink(); ?>' title='<?php  esc_html_e( "Permalink to", "intense" ); ?>  <?php echo the_title_attribute( 'echo=0' ); ?>' rel='bookmark'>
					<h1 class='entry-title' itemprop='name'>
						<?php echo the_title_attribute( 'echo=0' ); ?>
					</h1>
				</a>
			</div>	
		</div>
	</div>

	<!-- Content -->
	<div class='intense row' style='<?php echo esc_attr( $intense_custom_post['cancel_plugin_layout_style'] ); ?> padding-top: 10px;'>
		<div class='entry-content intense col-lg-4 col-md-4 col-sm-12 col-xs-12'>
			<label><strong><?php echo __('Yield', 'intense' ) ?>: </strong> <span itemprop='yield'><?php the_field('intense_recipe_yield'); ?></span></label>
		</div>
		<div class='entry-content intense col-lg-4 col-md-4 col-sm-12 col-xs-12'>
			<label><strong><?php echo __('Calories', 'intense' ) ?>: </strong> <span itemprop='calories'><?php the_field('intense_recipe_calories'); ?></span></label>
		</div>		
	</div>
	<div class='intense row' style='<?php echo esc_attr( $intense_custom_post['cancel_plugin_layout_style'] ); ?> padding-top: 10px;'>
		<div class='entry-content intense col-lg-4 col-md-4 col-sm-12 col-xs-12'>
			<label><strong><?php echo __('Prep Time', 'intense' ) ?>:</strong> <?php echo intense_convert_minutes_to_ISO_format( get_field('intense_recipe_prep_time'), 'prepTime' ); ?></label>
		</div>
		<div class='entry-content intense col-lg-4 col-md-4 col-sm-12 col-xs-12'>
			<label><strong><?php echo __('Cook Time', 'intense' ) ?>: </strong><?php echo intense_convert_minutes_to_ISO_format( get_field('intense_recipe_cook_time'), 'cookTime' ); ?></label>
		</div>
		<div class='entry-content intense col-lg-4 col-md-4 col-sm-12 col-xs-12'>
			<label><strong><?php echo __('Ready In', 'intense' ) ?>: </strong><?php echo intense_convert_minutes_to_ISO_format( ( get_field('intense_recipe_prep_time') + get_field('intense_recipe_cook_time') ), 'totalTime' ); ?></label>
		</div>
	</div>
	<div class='intense row' style='<?php echo esc_attr( $intense_custom_post['cancel_plugin_layout_style'] ); ?> padding-top: 10px;'>
		<div class='entry-content intense col-lg-12 col-md-12 col-sm-12 col-xs-12' itemprop='summary'>
			<?php 
				$content = intense_template_content( 'intense_recipes', 'full_post', '', 100 ); 
				echo $content;
			?>
		</div>
	</div>
	<div class='intense row' style='<?php echo esc_attr( $intense_custom_post['cancel_plugin_layout_style'] ); ?> padding-top: 10px;'>
		<div class='entry-content intense col-lg-12 col-md-12 col-sm-12 col-xs-12'>
		<?php			

			$rows = get_field('intense_recipe_ingredients');
			if( $rows )
			{
				echo '<label><strong>' . __('Ingredients', 'intense' ) . ':</strong></label>';

				echo '<span itemprop="ingredient" itemscope itemtype="http://data-vocabulary.org/RecipeIngredient"><ul>';
			 
				foreach( $rows as $row )
				{
					$ingredient = $row['intense_recipe_ingredient'];
					echo '<li><span itemprop="amount">' . $row['intense_recipe_amount'] . ' ' . $row['intense_recipe_measurement'] . '</span> <span itemprop="name">' . $ingredient->name . '</span> ' . $row['intense_recipe_note'] . '</li>';
				}
			 
				echo '</ul></span>';
			}
		?>
		</div>
	</div>
	<div class='intense row' style='<?php echo esc_attr( $intense_custom_post['cancel_plugin_layout_style'] ); ?> padding-top: 10px;'>
		<div class='entry-content intense col-lg-12 col-md-12 col-sm-12 col-xs-12'>
		<?php			
			$rows = get_field('intense_recipe_instructions');
			$rowcount = 1;

			if( $rows )
			{
				$shortcode = '[intense_row padding_top="0"]';
				$shortcode .= '[intense_column size="12" medium_size="12"]<label><strong>' . __('Instructions', 'intense' ) . ':</strong></label>[/intense_column]';
				$shortcode .= '[/intense_row]';
				$shortcode .= '<div itemprop="instructions">';
				$shortcode .= '<ol>';

				foreach( $rows as $row )
				{
					$instructions = $row['intense_recipe_description'];
					$shortcode .= '<li>';

					if ( $row['intense_recipe_image'] ) {
						$image = $row['intense_recipe_image'];
						$shortcode .= '<div class="pull-right" style="margin-left: 20px; margin-bottom:20px;">[intense_image image="' . $image['url'] . '" size="square150"]</div>';
						$shortcode .= $instructions;
						$shortcode .= '<div style="clear:both;"> </div>';

					} else {
						$shortcode .= $instructions;					
					}

					$shortcode .= '</li>';
					$rowcount++;
				}
			 
			 	$shortcode .= '</ol>';
			 	$shortcode .= '</div>';

				echo do_shortcode( $shortcode );
			}
		?>
		</div>
	</div>
	<?php
	$cuisine = get_the_term_list( $post->ID, 'intense_recipes_cuisines', '', ', ', '' );
	$course = get_the_term_list( $post->ID, 'intense_recipes_courses', '', ', ', '' );
	$skill_level = get_the_term_list( $post->ID, 'intense_recipes_skill_levels', '', ', ', '' );
	?>
	<!-- Footer -->
	<footer style='padding-top: 5px;'>
		<hr style="margin: 10px 0;">
		<div class='intense row' style='<?php echo esc_attr( $intense_custom_post['cancel_plugin_layout_style'] ); ?> padding-top: 5px;'>
			<div class='entry-content intense col-lg-4 col-md-4 col-sm-12 col-xs-12'>
				<label><strong><?php echo __('Cuisine', 'intense' ) ?>:</strong> <?php echo $cuisine; ?></label>
			</div>
			<div class='entry-content intense col-lg-4 col-md-4 col-sm-12 col-xs-12'>
				<label><strong><?php echo __('Course', 'intense' ) ?>: </strong><?php echo $course; ?></label>
			</div>
			<div class='entry-content intense col-lg-4 col-md-4 col-sm-12 col-xs-12'>
				<label><strong><?php echo __('Skill Level', 'intense' ) ?>: </strong><?php echo $skill_level; ?></label>
			</div>
		</div>
	</footer>
	<?php echo $intense_custom_post['animation_wrapper_end']; ?>
</article>

<?php

	$i++;
endwhile;
do_action('intense_after_main_content');
}
remove_action( 'ultimatum_loop', 'ultimatum_standard_loop' );
add_action('ultimatum_loop', 'ult_intense_recipe');
ultimatum();
