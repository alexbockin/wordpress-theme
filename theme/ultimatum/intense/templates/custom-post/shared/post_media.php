<?php
/*
Intense Template Name: Post Media
*/

$intense_custom_post = Intense_Custom_Post::get_metadata();

if ( !isset( $include_link ) ) $include_link = 1;
if ( !isset( $lightbox_type ) ) $lightbox_type = null;
if ( !isset( $is_slider ) ) $is_slider = 1;

if ( $intense_custom_post['template_link_url'] == null ) $include_link = 0;

if ( is_object( $intense_custom_post['post_type'] ) ) {
	$custom_post_image_field = $intense_custom_post['post_type']->get_image_field();
}

if ( isset( $show_only_first ) ) {
	$show_only_first = $show_only_first;
} else if ( isset( $intense_custom_post['show_only_first_image'] ) ) {
	$show_only_first = $intense_custom_post['show_only_first_image'];
} else {
	$show_only_first = false;
}

if ( $intense_custom_post['show_images'] ) {
	echo '<div class="image"' . ( isset( $padding ) ? 'style="padding: ' . $padding . ';"' : '' ) . '>' .
	intense_run_shortcode( 'intense_post_media', array(
		'size' => $size,
		'image_type' => intense_coalesce( get_field( 'intense_featured_image_type' ), 'standard' ),
		'shadow' => $intense_custom_post['image_shadow'],
		'hover_effect_type' => $intense_custom_post['hover_effect_type'],
		'effeckt' => $intense_custom_post['hover_effect'],
		'effeckt_color' => $intense_custom_post['hover_effect_color'],
		'effeckt_opacity' => $intense_custom_post['hover_effect_opacity'],
		'subtle_effect' => $intense_custom_post['subtle_effect'],
		'border_radius' => $intense_custom_post['image_border_radius'],
		'is_slider' => $is_slider,
		'include_link' => $include_link,
		'custom_post_field' => isset( $custom_post_image_field ) ? $custom_post_image_field : null,
		'template_link_url' => esc_url( $intense_custom_post['template_link_url'] ),
		'show_missing_image' => $intense_custom_post['show_missing_image'],
		'show_only_first' => $show_only_first,
		'lightbox_type' => $lightbox_type,
		'template' => isset( $template ) ? $template : null
	) ) .
	'</div>';
}
