<?php
/*
Intense Template Name: Menu (Two Columns)
*/

$post = get_post(); 
$intense_custom_post = Intense_Custom_Post::get_metadata();

$inline = ( is_sticky() && 'inline' == $intense_custom_post['sticky_mode'] );

if ( $inline ) {
	$span = "12";
	$size = "postWide";
	$header_tag = "h4";
} else {
	$span = "6";
	$size = ( isset( $intense_custom_post['image_size'] ) ? $intense_custom_post['image_size'] : 'medium640' );
	$header_tag = "h4";
}

$padding_bottom = Intense()->options['intense_layout_row_default_padding']['padding-top'];
$intense_post_type = $intense_custom_post['post_type'];
?>
<div class='intense col-lg-<?php echo esc_attr( $span ); ?> col-md-<?php echo esc_attr( $span ); ?> col-sm-6 col-xs-12  <?php echo esc_attr( $intense_custom_post['post_classes'] ); ?> intense_post nogutter' style='margin-top:0px; margin-left: 0px; float: none; padding: 0 10px; padding-bottom: 0; display:inline-block; vertical-align: top;'>
	<article id='post-<?php echo $post->ID; ?>' class='<?php echo ( esc_attr( $inline ) ?  'featured ' : '' ); ?>'>
	<?php echo $intense_custom_post['animation_wrapper_start']; ?>
		<div class="intense_single_menu_item" style="border-top: 1px dashed #cccccc; border-bottom: 1px dashed #cccccc; padding:5px;">
			<div class="intense row" style="padding-top: 0; padding-bottom: 0; margin-top: 0; margin-bottom: 0; ">
				<div class="intense col-lg-9 col-xs-12 col-sm-9 col-md-9">
					<?php if ( $intense_custom_post['show_images'] && !empty( intense_get_post_thumbnail_ids( $post->id ) ) ) { ?>
						<!-- Image -->
						<div>
							<div style='width:45px; float:left; display:block;'>
								<?php echo intense_get_template( '/custom-post/shared/post_media.php', array( 'size' => 'thumbnail', 'border_radius' => '50%', 'include_link' => 0, 'custom_post_image_field' => ( isset( $custom_post_image_field ) ? $custom_post_image_field : null ) ) ); ?>
							</div>
							<div style="padding-left:55px;">
								<?php echo intense_get_template( '/custom-post/shared/post_title.php', array( 'tag' => $header_tag, 'margin' => '5px 0' ) ); ?>
								
								<div class='entry-content' style="padding-bottom:5px;">
									<?php echo get_field('intense_menu_items_description'); ?>
								</div>
							</div>
							<div style="clear:both;"></div>
						</div>
					<?php } else { ?>
							<?php echo intense_get_template( '/custom-post/shared/post_title.php', array( 'tag' => $header_tag, 'margin' => '5px 0' ) ); ?>
							
							<div class='entry-content' style="padding-bottom:5px;">
								<?php echo get_field('intense_menu_items_description'); ?>
							</div>

					<?php } ?>
				</div>
				<div class="intense col-lg-3 col-xs-12 col-sm-3 col-md-3">
					<?php
						$rows = get_field('intense_menu_items_prices');
						if( $rows )
						{
							foreach( $rows as $row )
							{
								$item_price = '';
								$item_description = '';

								if ( !empty( $row['intense_menu_items_description'] ) ) {
									$item_description = '<em style="padding-right:30px;">' . $row['intense_menu_items_description'] . '</em>';
								}

								if ( !empty( $row['intense_menu_items_price'] ) ) {
									$item_price = '<' . $header_tag . ' class="entry-title" style="margin:5px 0 0 0; display:inline-block;">' . $row['intense_menu_items_price'] . '</' . $header_tag . '><br />';
								}

								echo '<div style="float:right;">' . $item_description . $item_price . '</div>';
							}
						}
					?>
				</div>
			</div>
		</div>
	<?php echo $intense_custom_post['animation_wrapper_end']; ?>
	</article>
</div>