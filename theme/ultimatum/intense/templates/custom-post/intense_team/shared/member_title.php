<?php
/*
Intense Template Name: Member Title
*/

$member_title = get_field( 'intense_member_title' );

if ( !empty( $member_title ) ) { 
	echo '<h4 style="margin: 0;">' . $member_title . '</h4>';
}
