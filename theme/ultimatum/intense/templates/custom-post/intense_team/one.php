<?php
/*
Intense Template Name: One Column
*/

$post = get_post(); 
$intense_custom_post = Intense_Custom_Post::get_metadata();

$intense_post_type = $intense_custom_post['post_type'];
?>
<article class='intense col-lg-12 col-md-12 col-sm-12 col-xs-12 <?php echo esc_attr( $intense_custom_post['post_classes'] ); ?> intense_post nogutter' style='<?php echo esc_attr( $intense_custom_post['plugin_layout_style'] ); ?>' id='post-<?php echo $post->ID; ?>'>
	<!-- Head -->
	<?php echo $intense_custom_post['animation_wrapper_start']; ?>
<div class='intense row' style='<?php echo esc_attr( $intense_custom_post['cancel_plugin_layout_style'] ); ?>'>
		<div class='intense col-lg-12 col-md-12 col-sm-12 col-xs-12' style="min-height:75px;">
			<?php 
				$imagesrc = intense_get_post_thumbnail_src( 'large1600', true, false );
   
  				if ( isset( $imagesrc ) ) {
					echo intense_run_shortcode( 'intense_content_section', array(
						'background_type' => 'image',
						'image' => $imagesrc[0],
						'imagesize' => 'large1600',
						'imagemode' => 'fixed',
						'height' => '250'
					) );
				}
			?>
		</div>
		<div class='intense col-lg-12 col-md-12 col-sm-12 col-xs-12' style='text-align:center; margin-top:-75px;'>
			<?php 
			$intense_custom_post['image_size'] = 'square150';
			$intense_custom_post['image_border_radius'] = '50%';			
			?>

			<?php echo intense_get_template( '/custom-post/shared/post_media.php', array( 'size' => 'square150', 'show_only_first' => '1' ) ); ?>

		</div>
		<div class='intense col-lg-12 col-md-12 col-sm-12 col-xs-12' style='padding-top:15px;'>
			<div style='text-align: center;'><h2><?php echo the_title_attribute( 'echo=0' ); ?></h2></div>
			<div style='text-align: center;'><h4><?php echo get_field( 'intense_member_title' ); ?></h4></div>
			<?php if( get_field( 'intense_member_facebook' ) ||
					  get_field( 'intense_member_googleplus' ) ||
					  get_field( 'intense_member_twitter' ) || 
					  get_field( 'intense_member_dribbble' ) || 
					  get_field( 'intense_member_linkedin' ) || 
					  get_field( 'intense_member_custom_social_icon' ) ) { ?>
			<div style='text-align: center; padding-bottom: 30px;'>
				
				<?php
					if( get_field( 'intense_member_facebook' ) ) {
						echo intense_run_shortcode( 'intense_social_icon', array( 
							'mode' => 'fontawesome',
							'type' => 'facebook',
							'link' => get_field( 'intense_member_facebook' ),
							'link_target' => '_blank',
							'size' => '20',
							'color' => 'primary'
						) );
					}

					if( get_field( 'intense_member_googleplus' ) ) {
						echo intense_run_shortcode( 'intense_social_icon', array( 
							'mode' => 'fontawesome',
							'type' => 'google-plus',
							'link' => get_field( 'intense_member_googleplus' ),
							'link_target' => '_blank',
							'size' => '20',
							'color' => 'primary'
						) );
					}

					if( get_field( 'intense_member_twitter' ) ) {
						echo intense_run_shortcode( 'intense_social_icon', array( 
							'mode' => 'fontawesome',
							'type' => 'twitter',
							'link' => get_field( 'intense_member_twitter' ),
							'link_target' => '_blank',
							'size' => '20',
							'color' => 'primary'
						) );
					}

					if( get_field( 'intense_member_dribbble' ) ) {
						echo intense_run_shortcode( 'intense_social_icon', array( 
							'mode' => 'fontawesome',
							'type' => 'dribbble',
							'link' => get_field( 'intense_member_dribbble' ),
							'link_target' => '_blank',
							'size' => '20',
							'color' => 'primary'
						) );
					}

					if( get_field( 'intense_member_linkedin' ) ) {
						echo intense_run_shortcode( 'intense_social_icon', array( 
							'mode' => 'fontawesome',
							'type' => 'linkedin',
							'link' => get_field( 'intense_member_linkedin' ),
							'link_target' => '_blank',
							'size' => '20',
							'color' => 'primary'
						) );
					}

					$socialImage = get_field( 'intense_member_custom_social_icon' );

					if( !empty( $socialImage ) ) {
						echo intense_run_shortcode( 'intense_social_icon', array( 
							'mode' => 'custom',
							'image' => $socialImage["id"],
							'link' => get_field( 'intense_member_custom_social_link' ),
							'link_target' => '_blank',
							'size' => '20',
							'color' => 'primary'
						) );
				    }

					if( get_field( 'intense_member_custom_social_icon' ) ) {
						echo intense_run_shortcode( 'intense_social_icon', array( 
							'mode' => 'custom',
							'image' => get_field( 'intense_member_facebook' ),
							'link' => get_field( 'intense_member_custom_social_link' ),
							'link_target' => '_blank',
							'size' => '20',
							'color' => 'primary'
						) );
					}
				?>

			</div>
			<?php } ?>
			<div >
				<?php echo intense_template_content( $intense_post_type, $intense_custom_post['template_content'], $intense_custom_post['template_content_length'], 120 ); ?>
			</div>
			<div class='entry-content'>
				
				<?php echo intense_get_template( '/custom-post/shared/post_read_more.php' ); ?>
				
			</div>
		</div>
	</div>
	<?php echo $intense_custom_post['animation_wrapper_end']; ?>
</article>
