<?php
/*
Intense Template Name: Two Columns (text on right)
*/

$intense_custom_post = Intense_Custom_Post::get_metadata();

$intense_custom_post['show_meta'] = 0;

echo intense_get_template( '/custom-post/post/two_text_right.php', array( 'after_title' => intense_get_template( '/custom-post/intense_team/shared/member_title.php' ) ) );