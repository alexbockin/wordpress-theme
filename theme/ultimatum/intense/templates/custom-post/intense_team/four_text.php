<?php
/*
Intense Template Name: Four Columns (text)
*/

$intense_custom_post = Intense_Custom_Post::get_metadata();

$intense_custom_post['show_meta'] = 0;

echo intense_get_template( '/custom-post/post/four_text.php', array( 'after_title' => intense_get_template( '/custom-post/intense_team/shared/member_title.php' ), 'after_content' => intense_get_template( '/custom-post/shared/post_social_icons.php' ) ) );