<?php
/*
Intense Template Name: One Column
*/

$post = get_post(); 
$intense_custom_post = Intense_Custom_Post::get_metadata();

$intense_post_type = $intense_custom_post['post_type'];
?>
<article class='intense col-lg-12 col-md-12 col-sm-12 col-xs-12 <?php echo esc_attr( $intense_custom_post['post_classes'] ); ?> intense_post nogutter' style='<?php echo esc_attr( $intense_custom_post['plugin_layout_style'] ); ?>' id='post-<?php echo $post->ID; ?>'>
	<!-- Head -->
	<?php echo $intense_custom_post['animation_wrapper_start']; ?>
	<div class='intense row' style='<?php echo esc_attr( $intense_custom_post['cancel_plugin_layout_style'] ); ?>'>
		<div class='intense col-lg-12 col-md-12 col-sm-12 col-xs-12' style='<?php echo esc_attr( $intense_custom_post['cancel_plugin_layout_style'] ); ?> padding-top:20px;'>
			<div class='intense col-lg-4 col-md-4 col-sm-12 col-xs-12' style='padding-left:0;'>
				<div class='post-header'>

					<?php echo intense_get_template( '/custom-post/shared/post_title.php', array( 'tag' => 'h2' ) ); ?>
					
					<?php echo intense_get_template( '/custom-post/shared/post_subtitle.php', array( 'tag' => 'h4' ) ); ?>

				</div>
			</div>
			<div class='intense col-lg-4 col-md-4 col-sm-12 col-xs-12' style='text-align:center;'>

				<?php echo intense_get_template( '/custom-post/shared/post_media.php', array( 'size' => 'medium800' ) ); ?>

			</div>
			<div class='intense col-lg-4 col-md-4 col-sm-12 col-xs-12'>
				<div class='pull-right'>
					<?php echo get_field( 'intense_client_address' ) . '<br /><a href="' . esc_url( get_field( 'intense_client_website' ) ) . '" target="_blank">' . esc_url( get_field( 'intense_client_website' ) ) . '</a>' ?>
				</div>
			</div>
		</div>
	</div>
		
	<?php if ( $intense_custom_post['show_images'] ) { ?>
	<!-- Image -->
	<div class='intense row' style='<?php echo esc_attr( $intense_custom_post['cancel_plugin_layout_style'] ); ?>'>
		<div class='intense col-lg-12 col-md-12 col-sm-12 col-xs-12' style='position: relative; padding-top:10px;'>
			<?php
				$imagesrc = intense_get_post_thumbnail_src( 'postWide', true, false );

				if ( isset( $imagesrc ) ) {
					echo intense_run_shortcode( 'intense_content_section', array( 
			          'background_type' => 'image',
			          'image' => esc_url( $imagesrc[0] ),
			          'height' => '250'
			        ) );
				}
			?>	
		</div>
	</div>
	<?php } ?>
	<?php echo $intense_custom_post['animation_wrapper_end']; ?>
</article>
