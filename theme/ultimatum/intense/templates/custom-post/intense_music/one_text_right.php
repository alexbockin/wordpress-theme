<?php
/*
Intense Template Name: One Column (text right)
*/

$post = get_post(); 
$intense_custom_post = Intense_Custom_Post::get_metadata();

$intense_post_type = $intense_custom_post['post_type'];

?>
<article class='intense row <?php echo esc_attr( $intense_custom_post['post_classes'] ); ?> intense_post nogutter' style='<?php echo esc_attr( $intense_custom_post['plugin_layout_style'] ); ?> margin:10px 0;' id='post-<?php echo $post->ID; ?>'>
	<?php echo $intense_custom_post['animation_wrapper_start']; ?>
		<!-- Image -->
		<div class='intense col-lg-5 col-md-5 col-sm-3 col-xs-12' style="margin-left: 0px; margin-right: 10px; padding:0;">
			<?php 
			$embed = get_field( 'intense_music_link' );
			$audio = get_field( 'intense_music_audio_clip' );

			if ( !empty( $embed ) ) {
				$var = apply_filters('the_content', "[embed]" . $embed . "[/embed]");
				echo $var;
			} else { ?>
				<div style="padding: 10px 0;">
					<?php echo intense_get_template( '/custom-post/shared/post_media.php', array( 'size' => intense_coalesce( $intense_custom_post['image_size'], 'medium800' ) ) ); ?>
				</div>

				<?php if ( !empty( $audio ) ) { ?>
					<div style='padding:5px 0;'>
						<?php echo intense_run_shortcode( 'intense_audio', array( 'url' => $audio ) ); ?>
					</div>
				<?php } ?>
			<?php } ?>
		</div>

		<?php if ( get_field( 'intense_music_purchase_link' ) != '' || $intense_custom_post['show_social_sharing'] ) { ?>
			<div class='pull-right' style='display:inline-block; text-align:middle;'>
				<?php if ( get_field( 'intense_music_video' ) != '' ) {
					$random = rand();
					$video_type = get_field( 'intense_music_video_type' );
					$video_url = get_field( 'intense_music_video' );

					$video = intense_run_shortcode( 'intense_video', array( 
						'video_type' => $video_type,
			        	'video_url' => $video_url
			        ) );

					$icon = intense_run_shortcode( 'intense_icon', array( 
						'source' => Intense()->options['intense_play_icon']['source'], 
						'type' => Intense()->options['intense_play_icon']['type'],
						'width' => '21',
						'height' => '21',
						'color' => 'primary'
					) );

					echo intense_run_shortcode( 'intense_lightbox', array(
			        	'type' => 'magnificpopup',
			        	'content_type' => 'html',
			        	'html_element' => 'video_trailer_' . $random,
			        ), $icon );

			        echo '<div id="video_trailer_' . $random . '" style="width:800px; height:450px;">' . $video . '</div>';
			    }

				echo intense_get_template( '/custom-post/shared/post_social_sharing.php' );

				if ( get_field( 'intense_music_purchase_link' ) != '' ) {
					$buy = intense_coalesce( get_field( 'intense_music_purchase_text' ), __( 'Buy', 'intense' ) );
					echo intense_run_shortcode( 'intense_button', array( 'size' => 'small', 'link' => get_field( 'intense_music_purchase_link' ), 'target' => '_blank', 'icon' => 'angle-right', 'icon_position' => 'right' ), $buy ); 
				} ?>
			</div>
		<?php } ?>

		<!-- Head -->
		<div class='post-header'>
		
			<?php echo intense_get_template( '/custom-post/shared/post_title.php', array( 'tag' => 'h2', 'margin' => '5px 0 0 0' ) ); ?>
			
			<?php echo intense_get_template( '/custom-post/shared/post_subtitle.php', array( 'tag' => 'h4', 'margin' => '5px 0' ) ); ?>			

		</div>
		
		<?php echo intense_get_template( '/custom-post/shared/post_metadata.php' ); ?>

		<?php
		$artists = get_the_term_list( $post->ID, 'intense_music_artist', '', ', ', '' );
		$record_labels = get_the_term_list( $post->ID, 'intense_music_label', '', ', ', '' );
		$catalog = get_the_term_list( $post->ID, 'intense_music_catalog', '', ', ', '' );
		$release = get_field( 'intense_music_release_date' );
		?>

		<?php if ( !empty( $artists ) || !empty( $release ) || !empty( $record_labels ) || !empty( $catalog ) ) { ?>
			<div class='intense music' style='padding-top: 10px;'>
				<?php if ( !empty( $artists ) ) { ?>
					<label style="display:block; font-weight: normal;"><strong style="text-transform:uppercase;"><?php echo __('Artist(s)', 'intense' ) ?>:</strong> <?php echo $artists; ?></label>
				<?php } ?>
				<?php if ( !empty( $release ) ) { ?>
					<label style="display:block; font-weight: normal;"><strong style="text-transform:uppercase;"><?php echo __('Release Date', 'intense' ) ?>:</strong> <?php echo date("M d, Y", strtotime( $release ) ); ?></label>
				<?php } ?>
				<?php if ( !empty( $record_labels ) ) { ?>
					<label style="display:block; font-weight: normal;"><strong style="text-transform:uppercase;"><?php echo __('Label(s)', 'intense' ) ?>: </strong><?php echo $record_labels; ?></label>
				<?php } ?>
				<?php if ( !empty( $catalog ) ) { ?>
					<label style="display:block; font-weight: normal;"><strong style="text-transform:uppercase;"><?php echo __('Catalog', 'intense' ) ?>: </strong><?php echo $catalog; ?></label>
				<?php } ?>
			</div>
		<?php } ?>

		<!-- Content -->	
		<div class='entry-content'>
			<?php 
				echo '<strong style="text-transform:uppercase;">' . __('Description', 'intense' ) . ':</strong> ' . intense_template_content( $intense_post_type, $intense_custom_post['template_content'], $intense_custom_post['template_content_length'], 45 );
			?>
		</div>
		
		<?php echo intense_get_template( '/custom-post/shared/post_read_more.php' ); ?>

		<div class="clearfix"></div>
	<?php echo $intense_custom_post['animation_wrapper_end']; ?>
</article>
