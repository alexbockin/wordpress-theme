<?php
/*
Intense Template Name: Timeline (text right)
*/

$post = get_post(); 
$intense_custom_post = Intense_Custom_Post::get_metadata();

$intense_post_type = $intense_custom_post['post_type'];
?>
<article class='intense row <?php echo esc_attr( $intense_custom_post['post_classes'] ); ?> nogutter' style='<?php echo esc_attr( $intense_custom_post['plugin_layout_style'] ); ?>' id='post-<?php echo $post->ID; ?>'>
	<!-- Image -->
	<div class='intense col-lg-7 col-md-12 col-sm-7 col-xs-7' style="margin-left: 0px; margin-right: 10px;">
		
		<?php echo intense_get_template( '/custom-post/shared/post_media.php', array( 'size' => intense_coalesce( $intense_custom_post['image_size'], 'postWide' ), 'custom_post_image_field' => ( isset( $custom_post_image_field ) ? $custom_post_image_field : null ) ) ); ?>

	</div>

	<!-- Head -->
	<div class='post-header'>
		
		<?php echo intense_get_template( '/custom-post/shared/post_title.php', array( 'tag' => 'h2' ) ); ?>
		
	</div>
	
	<?php echo intense_get_template( '/custom-post/shared/post_metadata.php' ); ?>

	<!-- Content -->	
	<div class='entry-content'>
		<?php echo intense_template_content( $intense_post_type, $intense_custom_post['template_content'], $intense_custom_post['template_content_length'], 30 ); ?>
	</div>
	<div class="clearfix"></div>

	<!-- Footer -->
	<footer style='padding-top: 5px;'>
		
		<?php echo intense_get_template( '/custom-post/shared/post_read_more.php' ); ?>

	</footer>
</article>
