<?php
/*
Intense Template Name: Timeline (text only)
*/

$post = get_post(); 
$intense_custom_post = Intense_Custom_Post::get_metadata();

$intense_post_type = $intense_custom_post['post_type'];
?>
<article class='intense row <?php echo esc_attr( $intense_custom_post['post_classes'] ); ?> nogutter' style='<?php echo esc_attr( $intense_custom_post['cancel_plugin_layout_style'] ); ?>' id='post-<?php echo $post->ID; ?>'>
	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
		<!-- Head -->
		<div class='intense row' style='<?php echo esc_attr( $intense_custom_post['cancel_plugin_layout_style'] ); ?>'>		
			<div class='intense col-lg-12 col-md-12 col-sm-12 col-xs-12'>		
				<div class='post-header'>
					
					<?php echo intense_get_template( '/custom-post/shared/post_title.php', array( 'tag' => 'h2' ) ); ?>
					
					<?php echo intense_get_template( '/custom-post/shared/post_metadata.php' ); ?>

				</div>	
			</div>
		</div>			

		<!-- Content -->
		<div class='intense row' style='<?php echo esc_attr( $intense_custom_post['cancel_plugin_layout_style'] ); ?>'>
			<div class='entry-content intense col-lg-12 col-md-12 col-sm-12 col-xs-12'>
				<?php echo intense_template_content( $intense_post_type, $intense_custom_post['template_content'], $intense_custom_post['template_content_length'], 50 ); ?>
			</div>
		</div>

		<!-- Footer -->
		<footer style='padding-top: 5px;'>
		
			<?php echo intense_get_template( '/custom-post/shared/post_read_more.php' ); ?>

		</footer>
	</div>
</article>
