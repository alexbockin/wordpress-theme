<?php
/*
Intense Template Name: One Column Thumbnail (text left)
*/

$post = get_post(); 
$intense_custom_post = Intense_Custom_Post::get_metadata();

$intense_post_type = $intense_custom_post['post_type'];
?>
<article class='intense col-lg-12 col-md-12 col-sm-12 col-xs-12 <?php echo esc_attr( $intense_custom_post['post_classes'] ); ?> intense_post nogutter' style='<?php echo esc_attr( $intense_custom_post['plugin_layout_style'] ); ?>' id='post-<?php echo $post->ID; ?>'>
	<?php echo $intense_custom_post['animation_wrapper_start']; ?>
			
	<div class='intense row' style='<?php echo esc_attr( $intense_custom_post['cancel_plugin_layout_style'] ); ?>'>
		<div class='entry-content intense col-lg-9 col-md-9 col-sm-8 col-xs-12'>
			<!-- Header -->
			<div class="post-header">
				<?php echo intense_get_template( '/custom-post/shared/post_title_author.php', array( 'tag' => 'h3', 'margin' => '0px 0px 10px 0px' ) ); ?>
				<?php echo intense_get_template( '/custom-post/shared/post_subtitle.php', array( 'tag' => 'h4', 'margin' => '0px 0px 10px 0px' ) ); ?>
				<?php echo intense_get_template( '/custom-post/shared/post_metadata.php' ); ?>
			</div>

			<!-- Content -->
			<?php echo intense_template_content( $intense_post_type, $intense_custom_post['template_content'], $intense_custom_post['template_content_length'], 100 ); ?>

			<?php echo intense_get_template( '/custom-post/shared/post_social_sharing.php' ); ?>
			<?php echo intense_get_template( '/custom-post/shared/post_read_more.php' ); ?>
		</div>
		
		<!-- Image -->
		<?php if ( $intense_custom_post['show_images'] ) { ?>
			<div class='intense col-lg-3 col-md-3 col-sm-4 col-xs-12' style='position: relative;'>
				<?php echo intense_get_template( '/custom-post/shared/post_media.php', array( 'size' => intense_coalesce( $intense_custom_post['image_size'], 'medium500' ), 'custom_post_image_field' => ( isset( $custom_post_image_field ) ? $custom_post_image_field : null ) ) ); ?>
			</div>
		<?php } ?>

	</div>
	
	<?php echo $intense_custom_post['animation_wrapper_end']; ?>
</article>