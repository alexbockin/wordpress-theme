<?php
/*
Intense Template Name: One Column Collapsible (no image or title)
*/

$post = get_post(); 
$intense_custom_post = Intense_Custom_Post::get_metadata();

$intense_post_type = $intense_custom_post['post_type'];
?>
<article class='intense col-lg-12 col-md-12 col-sm-12 col-xs-12 <?php echo esc_attr( $intense_custom_post['post_classes'] ); ?> intense_post nogutter' style='<?php echo esc_attr( $intense_custom_post['plugin_layout_style'] ); ?>' id='post-<?php echo $post->ID; ?>'>
	<!-- Head -->
	<?php echo $intense_custom_post['animation_wrapper_start']; ?>
	<div class='intense row' style='<?php echo esc_attr( $intense_custom_post['cancel_plugin_layout_style'] ); ?>'>
		<?php if ( $intense_custom_post['show_author'] && !is_author() ) { 
			$author_bio = intense_get_author_bio( get_the_author_meta( 'ID' ), 'one_image', 'square75' );
		?>
			<?php if ( !is_rtl() ) { ?>
			<div class='intense col-lg-1 col-md-1 col-sm-1 col-xs-1 author-image'>
					<a href='<?php echo get_author_posts_url( get_the_author_meta( 'ID' ) ); ?>' rel='author'><img width="50" height="50" src="<?php echo $author_bio['image']; ?>"/></a>
			</div>
			<?php } ?>
		<div class='intense col-lg-11 col-md-11 col-sm-11 col-xs-11'>
		<?php } else { ?>
		<div class='intense col-lg-12 col-md-12 col-sm-12 col-xs-12'>
		<?php } ?>
			<div class='post-header'>
				
				<?php echo intense_get_template( '/custom-post/shared/post_subtitle.php', array( 'tag' => 'h4' ) ); ?>
				
				<?php echo intense_get_template( '/custom-post/shared/post_metadata.php' ); ?>

			</div>	
		</div>

		<?php if ( is_rtl() ) { ?>
			<div class='intense col-lg-1 col-md-1 col-sm-1 col-xs-1 author-image'>
					<a href='<?php echo get_author_posts_url( get_the_author_meta( 'ID' ) ); ?>' rel='author'><?php echo get_avatar( get_the_author_meta( 'ID' ), 50 ); ?></a>
			</div>
		<?php } ?>
	</div>

	<!-- Content -->
	<div class='intense row' style='<?php echo esc_attr( $intense_custom_post['cancel_plugin_layout_style'] ); ?>'>
		<div class='entry-content intense col-lg-12 col-md-12 col-sm-12 col-xs-12'>
			<?php echo intense_template_content( $intense_post_type, $intense_custom_post['template_content'], $intense_custom_post['template_content_length'], 100 ); ?>
		</div>
	</div>

	<!-- Footer -->
	<footer style='padding-top: 5px;'>
		
		<?php echo intense_get_template( '/custom-post/shared/post_social_sharing.php' ); ?>
		
		<?php echo intense_get_template( '/custom-post/shared/post_read_more.php' ); ?>

	</footer>
	<?php echo $intense_custom_post['animation_wrapper_end']; ?>
</article>
