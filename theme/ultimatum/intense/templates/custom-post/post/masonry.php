<?php
/*
Intense Template Name: Masonry (minimalistic)
*/

$post = get_post(); 
$intense_custom_post = Intense_Custom_Post::get_metadata();

$intense_post_type = $intense_custom_post['post_type'];
?>
<article  id="post-<?php echo $post->ID; ?>" class="<?php echo esc_attr( $intense_custom_post['post_classes'] ); ?> intense_post" style="background: #efefef; box-shadow: 0px 0px 3px rgba(0,0,0,.4);" >
	<!-- Head -->
	<?php echo $intense_custom_post['animation_wrapper_start']; ?>

	<?php echo intense_get_template( '/custom-post/shared/post_media.php', array( 'size' => intense_coalesce( $intense_custom_post['image_size'], 'postWide' ) ) ); ?>

	<div style="padding: 20px;">
		<div class='post-header'>

			<?php echo intense_get_template( '/custom-post/shared/post_title.php', array( 'tag' => 'h2', 'margin' => '0' ) ); ?>
			
		</div>

		<?php echo intense_template_content( $intense_post_type, $intense_custom_post['template_content'], $intense_custom_post['template_content_length'], 20 ); ?>

		<!-- Footer -->
		<footer style='padding-top: 5px;'>
			
			<?php echo intense_get_template( '/custom-post/shared/post_social_sharing.php' ); ?>
			
			<?php echo intense_get_template( '/custom-post/shared/post_read_more.php' ); ?>

			<div class="clearfix"></div>
		</footer>
	</div>
	<?php echo $intense_custom_post['animation_wrapper_end']; ?>
</article>
