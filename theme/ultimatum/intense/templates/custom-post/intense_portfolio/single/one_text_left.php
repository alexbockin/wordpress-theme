<?php
/*
Intense Template Name: One Column (text left)
*/
function ult_intense_portfoliol()
{
	$post = get_post();
	$intense_custom_post = Intense_Custom_Post::get_metadata();

	$padding_top = Intense()->options['intense_layout_row_default_padding']['padding-top'];
	$padding_bottom = Intense()->options['intense_layout_row_default_padding']['padding-bottom'];
	$margin_top = Intense()->options['intense_layout_row_default_margin']['margin-top'];
	$margin_bottom = Intense()->options['intense_layout_row_default_margin']['margin-bottom'];

	$layout_style = '';

	if (!empty($padding_top)) {
		$layout_style .= 'padding-top: ' . $padding_top . '; ';
	}

	if (!empty($padding_bottom)) {
		$layout_style .= 'padding-bottom: ' . $padding_bottom . '; ';
	}

	if (!empty($margin_top)) {
		$layout_style .= 'margin-top: ' . $margin_top . '; ';
	}

	if (!empty($margin_bottom)) {
		$layout_style .= 'margin-bottom: ' . $margin_bottom . '; ';
	}

	$no_layout_style = "padding-top: 0px; padding-bottom: 0px; margin-top: 0px; margin-bottom: 0px;";

	$intense_post_type = Intense()->post_types->get_post_type('intense_portfolio');

	$intense_custom_post = array(
		'plugin_layout_style' => $layout_style,
		'cancel_plugin_layout_style' => $no_layout_style,
		'post_type' => $intense_post_type,
		'taxonomy' => '',
		'template' => '',
		'categories' => '',
		'exclude_categories' => '',
		'exclude_post_ids' => '',
		'authors' => '',
		'order_by' => '',
		'order' => '',
		'posts_per_page' => '',
		'image_size' => 'Full',
		'image_shadow' => '0',
		'hover_effect_type' => '',
		'hover_effect' => '',
		'subtle_effect' => '',
		'show_all' => '',
		'show_meta' => '0',
		'show_author' => '0',
		'infinite_scroll' => '',
		'show_filter' => '',
		'show_images' => '1',
		'show_missing_image' => '0',
		'show_social_sharing' => '1',
		'show_read_more' => '1',
		'read_more_text' => 'Read More',
		'timeline_mode' => '',
		'timeline_showyear' => '',
		'timeline_readmore' => '',
		'timeline_color' => '',
		'filter_easing' => '',
		'filter_effects' => '',
		'hover_effect_color' => '',
		'hover_effect_opacity' => '',
		'sticky_mode' => '',
		'image_border_radius' => '',
		'timeline_border_radius' => '',
		'animation_type' => '',
		'animation_trigger' => null,
		'animation_scroll_percent' => null,
		'animation_delay' => null,
		'animation_wrapper_start' => null,
		'animation_wrapper_end' => null,
		'is_slider' => ''
	);

	$i = 0;

	do_action('intense_before_main_content');
	$title = get_the_title();
	$title = apply_filters('intense/single_post_template/title', $title);

	while (have_posts()) : the_post();
		$item_classes = '';
		$intense_custom_post['image_shadow'] = '';
		$intense_custom_post['hover_effect_type'] = '';
		$intense_custom_post['hover_subtle_effect_type'] = '';
		$intense_custom_post['hover_effect'] = '';
		$intense_custom_post['hover_effect_color'] = '';
		$intense_custom_post['hover_effect_opacity'] = '';
		$intense_custom_post['index'] = $i;
		$hover = 'effeckt';

		if (has_post_thumbnail()) {
			if (get_field('intense_image_shadow') != '') {
				$intense_custom_post['image_shadow'] = get_field('intense_image_shadow');
			}

			if (get_field('intense_hover_effect_type') != '') {
				$intense_custom_post['hover_effect_type'] = get_field('intense_hover_effect_type');
				$hover = get_field('intense_hover_effect_type');
			}

			if (get_field('intense_hover_effect') != '' && $hover == 'effeckt') {
				$intense_custom_post['hover_effect'] = get_field('intense_hover_effect');

				if (get_field('intense_effect_color') != '') {
					$intense_custom_post['hover_effect_color'] = get_field('intense_effect_color');
				}

				if (get_field('intense_effect_opacity') != '') {
					$intense_custom_post['hover_effect_opacity'] = get_field('intense_effect_opacity');
				}
			} else {
				$intense_custom_post['hover_effect'] = get_field('intense_subtle_hover_effect');
			}
		}

		if (current_user_can('manage_options')) {
			$intense_custom_post['edit_link'] = "&nbsp;" . intense_run_shortcode('intense_button', array('size' => 'mini', 'color' => 'inverse', 'class' => 'post-edit-link', 'link' => get_edit_post_link($post->ID), 'title' => __("Edit Post", "intense")), __("Edit", "intense"));
		} else {
			$intense_custom_post['edit_link'] = '';
		}

		$intense_custom_post['post_classes'] = $item_classes;
		?>

		<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
			<div class="entry-content">
				<h1 class="entry-title"><?php echo $title; ?><?php edit_post_link(__('Edit', 'intense'), '<span class="btn btn-mini btn-inverse">', '</span>'); ?></h1>
				<?php
				if (get_field('intense_portfolio_subtitle') != '') {
					echo "<h4 style='font-style:italic;'>" . get_field('intense_portfolio_subtitle') . "</h4>";
				}
				?>
				<div class="intense row" style="padding-top: 0;">
					<div class="intense col-lg-4 col-xs-12 col-sm-12 col-md-4">
						<?php
						$categories = wp_get_post_terms($post->ID, 'portfolio_category', array("fields" => "all"));

						if ($categories) {
							echo "<h3>" . _n('Category', 'Categories', count($categories), 'intense') . "</h3>";

							$category_list = array();

							foreach ($categories as $category) {
								$category_list[] = $category->name;
							}

							echo join(', ', $category_list);
						}

						$skills = wp_get_post_terms($post->ID, 'portfolio_skills', array("fields" => "all"));

						if ($skills) {
							echo "<h3>" . _n('Skill', 'Skills', count($skills), 'intense') . "</h3>";

							$skill_list = array();

							foreach ($skills as $skill) {
								$skill_list[] = $skill->name;
							}

							echo join(', ', $skill_list);
						}

						echo "<hr>";

						if (get_field('intense_designed_by') != '') {
							echo __('Designed By', 'intense') . ": " . get_field('intense_designed_by') . "<br />";
						}

						if (get_field('intense_built_by') != '') {
							echo __('Built By', 'intense') . ": " . get_field('intense_built_by') . "<br />";
						}

						if (get_field('intense_produced_by') != '') {
							echo __('Produced By', 'intense') . ": " . get_field('intense_produced_by') . "<br />";
						}

						// if ( get_field( 'intense_portfolio_date' ) != '' ) {
						// 	echo __( 'Completion Date', 'intense' ) . ": " . date("M d, Y", strtotime( get_field( 'intense_portfolio_date' ) ) );
						// }

						if (get_field('intense_year_completed') != '') {
							echo __('Year Completed', 'intense') . ": " . get_field('intense_year_completed') . "<br />";
						}
						?>
					</div>
					<div class="intense col-lg-8 col-xs-12 col-sm-12 col-md-8">
						<?php echo intense_run_shortcode('intense_post_media', array(
							'size' => 'medium800',
							'image_type' => intense_coalesce(get_field('intense_featured_image_type'), 'standard'),
							'shadow' => $intense_custom_post['image_shadow'],
							'hover_effect_type' => $intense_custom_post['hover_effect_type'],
							'effeckt' => $intense_custom_post['hover_effect'],
							'effeckt_color' => $intense_custom_post['hover_effect_color'],
							'effeckt_opacity' => $intense_custom_post['hover_effect_opacity'],
							'subtle_effect' => $intense_custom_post['subtle_effect'],
							'is_slider' => true
						));
						?>
					</div>
				</div>
				<hr/>
				<?php
				$content = intense_template_content('intense_portfolio', 'full_post', '', 100);
				echo $content;
				?>
			</div>
		</article>

		<?php
		$i++;
	endwhile;

	do_action('intense_after_main_content');
}
remove_action( 'ultimatum_loop', 'ultimatum_standard_loop' );
add_action('ultimatum_loop', 'ult_intense_portfoliol');
ultimatum();
