<?php
/*
Intense Template Name: Three Columns (text)
*/

$post = get_post(); 
$intense_custom_post = Intense_Custom_Post::get_metadata();

$inline = ( is_sticky() && 'inline' == $intense_custom_post['sticky_mode'] );

if ( $inline ) {
	$span = "12";
	$size = "postWide";
	$header_tag = "h3";
} else {
	$span = "4";
	$size = ( isset( $intense_custom_post['image_size'] ) ? $intense_custom_post['image_size'] : 'medium640' );
	$header_tag = "h4";
}

$padding_bottom = Intense()->options['intense_layout_row_default_padding']['padding-top'];
$intense_post_type = $intense_custom_post['post_type'];
?>
<div class='intense col-lg-<?php echo esc_attr( $span ); ?> col-md-<?php echo esc_attr( $span ); ?> col-sm-12 col-xs-12 <?php echo esc_attr( $intense_custom_post['post_classes'] ); ?> intense_post nogutter' style='margin-left: 0px; float: none; padding: 0 10px; padding-bottom: <?php echo esc_attr( $padding_bottom ); ?>; display:inline-block; vertical-align: top;'>
	<article id='post-<?php echo $post->ID; ?>' class='<?php echo ( esc_attr( $inline ) ?  'featured ' : '' ); ?>'>
	<?php echo $intense_custom_post['animation_wrapper_start']; ?>
		
		<?php echo intense_get_template( '/custom-post/shared/post_media.php', array( 'size' => $size ) ); ?>
		
		<?php echo intense_get_template( '/custom-post/shared/post_title.php', array( 'tag' => $header_tag ) ); ?>

		<div class='post-category'>
			<h6><?php 
			$categories = explode(", ", $intense_custom_post['categories']);

			foreach ($categories as $category) {
				echo intense_run_shortcode( 'intense_badge', null, $category ) . ' '; 
			}
			?></h6>
		</div>
		<div class='entry-content'>
			<?php echo intense_template_content( $intense_post_type, $intense_custom_post['template_content'], $intense_custom_post['template_content_length'], 20 ); ?>
		</div>
		
		<?php echo intense_get_template( '/custom-post/shared/post_read_more.php' ); ?>

	<?php echo $intense_custom_post['animation_wrapper_end']; ?>
	</article>
</div>