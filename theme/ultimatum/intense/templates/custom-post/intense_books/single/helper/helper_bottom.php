<?php

/**
 * A helper function used in multiple recipe templates
 *
 * @param string  $template template name
 */

do_action( 'intense_after_main_content' );

get_footer( 'intense' );
