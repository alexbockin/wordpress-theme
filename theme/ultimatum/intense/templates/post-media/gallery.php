<?php
/*
Intense Template Name: Gallery with large featured image
*/

$intense_post_media = Intense_Post_Media::get_metadata();

$image_ids = array();

foreach ( $intense_post_media['image']['images'] as $key => $value) {
	$image_ids[] = $value['id'];
}

if ( empty( $intense_post_media['first_image_size'] ) )	$intense_post_media['first_image_size'] = 'large1600';
if ( empty( $intense_post_media['columns'] ) )	$intense_post_media['columns'] = 4;
if ( empty( $intense_post_media['marginpercent'] ) )	$intense_post_media['marginpercent'] = 1;
if ( empty( $intense_post_media['type'] ) )	$intense_post_media['type'] = 'standard';

echo intense_run_shortcode( 'intense_gallery', array(
 	'include' => join( $image_ids, ',' ),
 	'size' => $intense_post_media['image']['size'],
 	'first_image_size' => $intense_post_media['first_image_size'], 	
 	'columns' => $intense_post_media['columns'],
 	'marginpercent' => $intense_post_media['marginpercent'],
 	'type' => $intense_post_media['type']
) );
